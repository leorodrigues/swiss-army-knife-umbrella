# SWISS ARMY KNIFE

This is an embedded general purpose development platform written in elixir/erlang.

## How to get it

```bash
$ git clone git@bitbucket.org:leorodrigues/swiss_army_knife_umbrella.git
```

## How to test it

At the root of the repository you just cloned, issue the following commands:

```bash
$ mix deps.get
$ mix all.coverage
```

A coverage report will be generated for each app under `<root>/apps`.

Each app has three different types of test `unit`, `integration`
and `prototype`. They can be executed under each app's directory
or from the root like so:

```sh
# Use these from the umbrella's root
$ mix all.proto
$ mix all.unit
$ mix all.integration

# Use these from under each app's directory
$ mix proto
$ mix unit
$ mix integration
```

To run the application on the local machine before building a release,
use any of the following commands:

```sh
# This will run the application and it will stay alive until SIGTERM.
$ mix run --no-halt

# This will allow you to invoke the API interactively
$ iex -S mix

# This can be used to quit the system
iex(1)> :init.stop
```

## How to release

Releases for each platform must be built under the target platforms (as of now).
This is a limitation inherited from Elixir itself.

Use the following commands to build a release:

```sh
# On a workstation or CI environment
$ MIX_ENV=dev mix release standalone

# On production
$ MIX_ENV=prod mix release standalone
```

When omitted, the default release name is `standalone`. The other possible
releases are described below:

- `standalone_mac_os`
- `standalone_windows`
- `standalone_termux`
- `embedded_raspberry_pi`

Be sure to match the release name with the respective platform for each release
may include a different set of components targeted at the reffered platform.

## Configuration

Currently, all runtime configuration is done through environment variables.

### Environment variables

- SAK_LOG_LEVEL: info (default), debug, warn, error
- SAK_BASE_DATA_DIR: Any valid system path with r/w permission (default is $HOME)

