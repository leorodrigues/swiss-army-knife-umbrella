defmodule CommonActivities.Basic.ContextualServer do

  defmacro __using__(options \\ [ ]) do
    unless Keyword.has_key?(options, :supervisor) do
      raise ArgumentError, "Option :supervisor is required."
    end

    supervisor = Keyword.get(options, :supervisor)

    quote do
      use GenServer

      alias CommonActivities.Basic.ContextualServerBehaviour
      alias CommonActivities.Basic.ContextualServer

      @behaviour ContextualServerBehaviour

      @before_compile ContextualServer

      @supervisor unquote(supervisor)

      import ContextualServer, only: [
        abcast: 3,
        call: 3,
        call: 2,
        cast: 2,
        multi_call: 4,
        multi_call: 3
      ]

      @impl GenServer
      def handle_call({request, context}, from, state) do
        run_task(:handle_contextual_call, context, [request, from, state])
      end

      @impl GenServer
      def handle_cast({request, context}, state) do
        run_task(:handle_contextual_cast, context, [request, state])
      end

      @impl ContextualServerBehaviour
      def handle_contextual_call(_request, _from, state) do
        {:reply, :ok, state}
      end

      @impl ContextualServerBehaviour
      def handle_contextual_cast(_request, state) do
        {:noreply, state}
      end

      defoverridable [
        handle_contextual_call: 3,
        handle_contextual_cast: 2
      ]
    end
  end

  defmacro __before_compile__(_env) do
    quote do
      defp run_task(function, context, args) do
        start_and_wait_for_task(@supervisor, make_call(context, function, args))
      end

      defp start_and_wait_for_task(supervisor, invocation) do
        Task.await(Task.Supervisor.async_nolink(supervisor, invocation))
      end

      defp make_call(context, function, args) do
        fn -> restore_context(context); apply(__MODULE__, function, args) end
      end

      defp restore_context(context) do
        Enum.each(Map.keys(context), fn k ->
          Process.put(k, Map.get(context, k))
        end)
      end
    end
  end

  def abcast(nodes \\ [node()], name, request) do
    GenServer.abcast(nodes, name, {request, get_context()})
  end

  def call(server_name, request, timeout \\ 5000) do
    GenServer.call(server_name, {request, get_context()}, timeout)
  end

  def cast(server_name, request) do
    GenServer.cast(server_name, {request, get_context()})
  end

  def multi_call(nodes \\ [ ], name, request, timeout \\ :infinity) do
    GenServer.multi_call(nodes, name, {request, get_context()}, timeout)
  end

  defp get_context() do
    Process.get_keys() |> Enum.reduce(%{ }, fn k, m ->
      Map.put(m, k, Process.get(k))
    end)
  end
end
