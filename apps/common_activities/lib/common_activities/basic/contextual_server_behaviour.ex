defmodule CommonActivities.Basic.ContextualServerBehaviour do

  @type from :: {pid(), tag :: term()}

  @callback handle_contextual_call(request :: term, from, state :: term) ::
    {:reply, reply, new_state}
    | {:reply, reply, new_state, timeout() | :hibernate | {:continue, continue_arg :: term()}}
    | {:noreply, new_state}
    | {:noreply, new_state, timeout() | :hibernate | {:continue, continue_arg :: term()}}
    | {:stop, reason, reply, new_state}
    | {:stop, reason, new_state}
    when reply: term, new_state: term, reason: term

  @callback handle_contextual_cast(request :: term, state :: term) ::
    {:noreply, new_state}
    | {:noreply, new_state, timeout() | :hibernate | {:continue, continue_arg :: term()}}
    | {:stop, reason :: term(), new_state}
    when new_state: term()

end
