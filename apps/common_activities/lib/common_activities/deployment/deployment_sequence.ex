defmodule CommonActivities.Deployment.DeploymentSequence do

  require Logger

  alias CommonActivities.Basic.ContextualServerBehaviour
  alias CommonActivities.Basic.ContextualServer
  alias CommonActivities.Deployment.Topics

  @default_pipeline Hermes.Piping.Inbound.CommandReplyPipeline

  defmacro __using__(options \\ [ ]) do

    unless Keyword.has_key?(options, :supervisor) do
      raise ArgumentError, "Missing :supervisor option"
    end

    unless Keyword.has_key?(options, :get_deployment_reply_topic) do
      raise ArgumentError, "Missing :get_deployment_reply_topic"
    end

    unless Keyword.has_key?(options, :put_deployment_reply_topic) do
      raise ArgumentError, "Missing :put_deployment_reply_topic"
    end

    review_deployment_command_topic = Keyword.get(
      options, :review_deployment_command_topic, Topics.review_deployment
    )

    get_deployment_command_topic = Keyword.get(
      options, :get_deployment_command_topic, Topics.get_deployment
    )

    put_deployment_command_topic = Keyword.get(
      options, :put_deployment_command_topic, Topics.put_deployment
    )

    get_deployment_reply_topic = Keyword.get(
      options, :get_deployment_reply_topic
    )

    put_deployment_reply_topic = Keyword.get(
      options, :put_deployment_reply_topic
    )

    supervisor = Keyword.get(options, :supervisor)

    pipeline = Keyword.get(options, :pipeline, @default_pipeline)

    inbox = make_inbox(Keyword.get(options, :inbox), __CALLER__.module)

    quote do
      alias CommonActivities.Deployment.DeploymentSequenceBehaviour

      alias CommonActivities.Basic.ContextualServer

      alias Hermes.Piping

      use ContextualServer, supervisor: unquote(supervisor)

      require Logger

      @behaviour DeploymentSequenceBehaviour

      @unknown_inquiry_result "Unknown deployment inquiry result."

      @unknown_attempt_result "Unknown deployment attempt result."

      @failed_inquiry_result "A deployment inquiry has resulted in failure."

      @failed_request_result "A deployment request has resulted in failure."

      @handling_msg "Handling message at entry-point."

      @self __MODULE__

      @inbox unquote(inbox)

      @pipeline unquote(pipeline)

      @review_deployment_command_topic unquote(review_deployment_command_topic)

      @get_deployment_command_topic unquote(get_deployment_command_topic)

      @put_deployment_command_topic unquote(put_deployment_command_topic)

      @get_deployment_reply_topic unquote(get_deployment_reply_topic)

      @put_deployment_reply_topic unquote(put_deployment_reply_topic)

      def handle_get_reply(topic, inbox, payload, options \\ [ ]) do
        Logger.debug(comment: @handling_msg, topic: topic, inbox: inbox)
        apply(@pipeline, :convey, [payload, add_business(options, :continue)])
      end

      def handle_put_reply(topic, inbox, payload, options \\ [ ]) do
        Logger.debug(comment: @handling_msg, topic: topic, inbox: inbox)
        apply(@pipeline, :convey, [payload, add_business(options, :finish)])
      end

      @spec begin :: :ok
      def begin do
        ContextualServer.call(@self, :begin)
      end

      def continue(payload, _options \\ [ ]) do
        ContextualServer.call(@self, {:try_to_continue, payload})
      end

      def finish(payload, _options \\ [ ]) do
        ContextualServer.call(@self, {:try_to_finish, payload})
      end

      @impl DeploymentSequenceBehaviour
      def on_deployment_started, do: :ok

      @impl DeploymentSequenceBehaviour
      def on_deployment_stalled, do: :ok

      @impl DeploymentSequenceBehaviour
      def on_already_deployed, do: :ok

      @impl DeploymentSequenceBehaviour
      def on_deployment_required, do: :ok

      @impl DeploymentSequenceBehaviour
      def on_deployment_succeeded, do: :ok

      @impl DeploymentSequenceBehaviour
      def on_deployment_request_failed(_), do: :ok

      @impl DeploymentSequenceBehaviour
      def on_deployment_inquiry_failed(_), do: :ok

      defoverridable [
        on_deployment_started: 0,
        on_deployment_stalled: 0,
        on_already_deployed: 0,
        on_deployment_required: 0,
        on_deployment_succeeded: 0,
        on_deployment_inquiry_failed: 1,
        on_deployment_request_failed: 1
      ]

      @spec start_link(keyword) :: :ignore | {:error, any} | {:ok, pid}
      def start_link(opts \\ [ ]) do
        Logger.debug(comment: "Starting link.", inbox: @inbox)

        args = Keyword.get(opts, :init_args, [ ])
        GenServer.start_link(@self, args, Keyword.put(opts, :name, @self))
      end

      def stop do
        Logger.debug(comment: "Stopping link.", inbox: @inbox)
        GenServer.stop(@self)
      end

      @impl GenServer
      @spec init(any) :: any
      def init(args \\ [ ]) do
        {:ok, Enum.into(args, %{ }), {:continue, :register_entrypoints}}
      end

      @impl GenServer
      def handle_continue(:register_entrypoints, state) do
        register_entrypoints()
        {:noreply, state}
      end

      @impl ContextualServerBehaviour
      def handle_contextual_call(:begin, _from, state) do
        inquire_deployment()
        {:reply, :ok, state}
      end

      @impl ContextualServerBehaviour
      def handle_contextual_call({:try_to_continue, payload}, _from, state) do
        try_to_continue(payload)
        {:reply, :ok, state}
      end

      @impl ContextualServerBehaviour
      def handle_contextual_call({:try_to_finish, payload}, _from, state) do
        try_to_finish(payload)
        {:reply, :ok, state}
      end

      defp register_entrypoints do
        register_entrypoint(@get_deployment_reply_topic, :handle_get_reply)
        register_entrypoint(@put_deployment_reply_topic, :handle_put_reply)
      end

      defp register_entrypoint(topic, method) do
        Logger.debug(comment: "Registering handler for topic.", topic: topic)
        Piping.subscribe_pipeline_handling(topic, @inbox, __MODULE__, method)
      end

      defp unregister_entrypoints do
        unregister_entrypoint(@get_deployment_reply_topic)
        unregister_entrypoint(@put_deployment_reply_topic)
      end

      defp unregister_entrypoint(topic) do
        Logger.debug(comment: "Unregistering handler for topic.", topic: topic)
        Piping.unsubscribe_pipeline_handling(topic, @inbox)
      end

      defp inquire_deployment do
        invoke_callback(:on_deployment_started)

        Logger.debug(
          comment: "Issuing deployment inquiry.",
          command_topic: @get_deployment_command_topic,
          reply_topic: @get_deployment_reply_topic
        )
        apply(Piping, :submit_command, [
          @get_deployment_reply_topic,
          @get_deployment_command_topic,
          invoke_callback(:get_app_key)
        ])
      end

      defp try_to_continue(:not_found) do
        invoke_callback(:on_deployment_required)

        Logger.debug(
          comment: "Issuing deployment request.",
          command_topic: @put_deployment_command_topic,
          reply_topic: @put_deployment_reply_topic
        )
        apply(Piping, :submit_command, [
          @put_deployment_reply_topic,
          @put_deployment_command_topic,
          invoke_callback(:get_app_manifest)
        ])
      end

      defp try_to_continue(%{phase: :staged}) do
        invoke_callback(:on_deployment_stalled)

        Logger.debug(
          comment: "Issuing deployment review.",
          command_topic: @review_deployment_command_topic
        )
        apply(Piping, :submit_void_command, [
          @review_deployment_command_topic,
          invoke_callback(:get_app_key)
        ])
      end

      defp try_to_continue(%{phase: :deployed}) do
        invoke_callback(:on_already_deployed)
      end

      defp try_to_continue({:error, reason}) do
        Logger.error(comment: @failed_inquiry_result, reason: reason)
        invoke_callback(:on_deployment_inquiry_failed, [reason])
      end

      defp try_to_continue(payload) do
        Logger.warning(comment: @unknown_inquiry_result, result: payload)
      end

      defp try_to_finish(%{phase: :staged}), do: :ok
      defp try_to_finish(%{phase: :deployed}) do
        invoke_callback(:on_deployment_succeeded)
      end

      defp try_to_finish({:error, reason}) do
        Logger.error(comment: @failed_request_result, reason: reason)
        invoke_callback(:on_deployment_request_failed, [reason])
      end

      defp try_to_finish(payload) do
        Logger.warning(comment: @unknown_attempt_result, result: payload)
      end

      defp invoke_callback(callback, args \\ [ ]) do
        Logger.debug(comment: "Trying to invoke :#{callback} callback.")
        apply(@self, callback, args)
      end

      defp add_business(options, function_name) do
        Keyword.put(options, :business_target, {__MODULE__, function_name})
      end
    end
  end

  defp make_inbox(nil, module), do: module |> to_kebab_case()
  defp make_inbox(inbox, _), do: inbox

  defp to_kebab_case(module) do
    module
      |> to_string()
      |> replace(~r/Elixir\./u, "")
      |> replace(~r/(.)([A-Z])/u, "\\1-\\2")
      |> replace(~r/\./u, "")
      |> String.downcase()
  end

  defp replace(input, regex, replacement) do
    Regex.replace(regex, input, replacement)
  end
end
