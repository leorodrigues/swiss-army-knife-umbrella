defmodule CommonActivities.Deployment.DeploymentSequenceBehaviour do
  @callback on_deployment_started() :: :ok
  @callback on_deployment_stalled() :: :ok
  @callback on_already_deployed() :: :ok
  @callback on_deployment_required() :: :ok
  @callback on_deployment_succeeded() :: :ok
  @callback on_deployment_inquiry_failed(any) :: :ok
  @callback on_deployment_request_failed(any) :: :ok
  @callback get_app_key :: map
  @callback get_app_manifest :: map

  @optional_callbacks [
    on_deployment_started: 0,
    on_deployment_stalled: 0,
    on_already_deployed: 0,
    on_deployment_required: 0,
    on_deployment_succeeded: 0,
    on_deployment_inquiry_failed: 1,
    on_deployment_request_failed: 1
  ]
end
