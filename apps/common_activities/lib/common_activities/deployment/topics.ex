defmodule CommonActivities.Deployment.Topics do
  import Hermes.DynamicTopics

  @domain "org.leonardo.sak"

  @get_deployment compute_command_topic(@domain, :deployments, :get)
  @spec get_deployment :: String.t
  def get_deployment, do: @get_deployment

  @put_deployment compute_command_topic(@domain, :deployments, :put)
  @spec put_deployment :: String.t
  def put_deployment, do: @put_deployment

  @review_deployment compute_command_topic(@domain, :deployments, :review)
  @spec review_deployment :: String.t
  def review_deployment, do: @review_deployment

end
