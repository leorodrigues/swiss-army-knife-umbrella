defmodule CommonActivities.Basic.ContextualServerTest do
  use ExUnit.Case
  @moduletag :prototype

  alias CommonActivities.System.DummyContextualServer
  alias CommonActivities.TestTasker

  setup do
    :meck.new([DummyContextualServer, Logger], [:passthrough])

    Task.Supervisor.start_link(name: TestTasker)

    DummyContextualServer.start_link()

    on_exit(fn -> :meck.unload end)
  end

  describe "CommonActivities.Basic.ContextualServer.call" do
    test "should invoke a callback with a process context restored" do
      Process.put(:number, 42)
      {"hello jack", 42} = DummyContextualServer.get_greeting("jack")
    end
  end

  describe "CommonActivities.Basic.ContextualServer.cast" do
    test "should invoke a callback with a process context restored" do
      Process.put(:number, 9000)
      DummyContextualServer.print_greeting("dr. shandra")
      log_line = [comment: "hello dr. shandra", number: 9000]
      :meck.wait(Logger, :__do_log__, [:info, log_line, :_, :_], 3000)
    end
  end
end
