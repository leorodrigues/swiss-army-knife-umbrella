defmodule CommonActivities.Deployment.DeploymentSequenceTest do
  use ExUnit.Case

  alias CommonActivities.Deployment.DummyDeploymentSequence
  alias CommonActivities.Deployment.TestTopics
  alias CommonActivities.Deployment.Topics

  alias CommonActivities.TestTasker

  alias Hermes.Piping.Inbound.CommandReplyPipeline

  alias Hermes.Piping

  @modules_to_spy_on [DummyDeploymentSequence, Logger]
  @modules_to_stub [Piping, CommandReplyPipeline]

  setup do
    :meck.new(@modules_to_spy_on, [:passthrough])
    :meck.new(@modules_to_stub)

    :meck.expect(Piping, :subscribe_pipeline_handling, fn _, _, _, _ -> :ok end)
    :meck.expect(Piping, :unsubscribe_pipeline_handling, fn _, _ -> :ok end)

    Task.Supervisor.start_link(name: TestTasker)

    start_supervised(DummyDeploymentSequence)
    on_exit(fn -> :meck.unload end)
  end

  describe "CommonActivities.Deployment.DeploymentSequence.handle_get_reply" do
    test "should just invoke the given pipeline" do
      :meck.expect(CommandReplyPipeline, :convey, fn _, _ -> :ok end)

      DummyDeploymentSequence.handle_get_reply("a", "b", :payload)

      :meck.wait(CommandReplyPipeline, :convey, :_, 5000)
    end
  end

  describe "CommonActivities.Deployment.DeploymentSequence.handle_put_reply" do
    test "should just invoke the given pipeline" do
      :meck.expect(CommandReplyPipeline, :convey, fn _, _ -> :ok end)

      DummyDeploymentSequence.handle_put_reply("a", "b", :payload)

      :meck.wait(CommandReplyPipeline, :convey, :_, 5000)
    end
  end

  describe "CommonActivities.Deployment.DeploymentSequence.begin" do
    test "should check if the deployment already exists" do
      :meck.expect(Piping, :submit_command, fn _, _, _ -> :ok end)

      DummyDeploymentSequence.begin

      :meck.wait(DummyDeploymentSequence, :on_deployment_started, [ ], 5000)

      args = [TestTopics.get_deployment_reply, Topics.get_deployment, :_]
      :meck.wait(Piping, :submit_command, args, 5000)

      DummyDeploymentSequence.stop
    end
  end

  describe "CommonActivities.Deployment.DeploymentSequence.continue" do
    test "should put a deployment if it does not exist yet" do
      :meck.expect(Piping, :submit_command, fn _, _, _ -> :ok end)

      DummyDeploymentSequence.continue(:not_found)

      :meck.wait(DummyDeploymentSequence, :on_deployment_required, [ ], 5000)

      args = [TestTopics.put_deployment_reply, Topics.put_deployment, :_]
      :meck.wait(Piping, :submit_command, args, 5000)

      DummyDeploymentSequence.stop
    end

    test "should become ready if the deployment is marked as deployed" do
      DummyDeploymentSequence.continue(%{phase: :deployed})
      :meck.wait(DummyDeploymentSequence, :on_already_deployed, [ ], 5000)

      DummyDeploymentSequence.stop
    end

    test "should review the deployment if it is marked as staged" do
      :meck.expect(Piping, :submit_void_command, fn _, _ -> :ok end)

      DummyDeploymentSequence.continue(%{phase: :staged})

      :meck.wait(DummyDeploymentSequence, :on_deployment_stalled, [ ], 5000)

      args = [Topics.review_deployment, :_]
      :meck.wait(Piping, :submit_void_command, args, 5000)

      DummyDeploymentSequence.stop
    end

    test "should just run a callback if the inquiry failed" do
      DummyDeploymentSequence.continue({:error, :fake_problem})

      :meck.wait(DummyDeploymentSequence, :on_deployment_inquiry_failed, [:fake_problem], 5000)

      DummyDeploymentSequence.stop
    end

    test "should warn if an inquiry result is unexpected" do
      DummyDeploymentSequence.continue("this is the wrong payload")

      log_line = [comment: :_, result: "this is the wrong payload"]
      :meck.wait(Logger, :__do_log__, [:warning, log_line, :_, :_], 3000)

      DummyDeploymentSequence.stop
    end
  end

  describe "CommonActivities.Deployment.DeploymentSequence.finish" do
    test "should do nothing if the deployment was only staged" do
      DummyDeploymentSequence.finish(%{phase: :staged})

      DummyDeploymentSequence.stop
    end

    test "should become ready if the deployment is marked as deployed" do
      DummyDeploymentSequence.finish(%{phase: :deployed})

      :meck.wait(DummyDeploymentSequence, :on_deployment_succeeded, [ ], 5000)

      DummyDeploymentSequence.stop
    end

    test "should just run a callback if the deployment request failed" do
      DummyDeploymentSequence.finish({:error, :fake_problem})

      :meck.wait(DummyDeploymentSequence, :on_deployment_request_failed, [:fake_problem], 5000)

      DummyDeploymentSequence.stop
    end

    test "should warn if a deployment result is unexpected" do
      DummyDeploymentSequence.finish("this is the wrong payload")

      log_line = [comment: :_, result: "this is the wrong payload"]
      :meck.wait(Logger, :__do_log__, [:warning, log_line, :_, :_], 3000)

      DummyDeploymentSequence.stop
    end
  end
end
