defmodule CommonActivities.System.DummyContextualServer do
  alias CommonActivities.Basic.ContextualServer

  use ContextualServer, supervisor: CommonActivities.TestTasker

  require Logger

  @self __MODULE__

  def get_greeting(name) do
    ContextualServer.call(@self, {:get_greeting, name})
  end

  def print_greeting(name) do
    ContextualServer.cast(@self, {:print_greeting, name})
  end

  def start_link(opts \\ [ ]) do
    args = Keyword.get(opts, :init_args, [ ])
    GenServer.start_link(@self, args, name: @self)
  end

  @impl true
  def init(args \\ [ ]) do
    {:ok, Enum.into(args, %{ })}
  end

  @impl true
  def handle_contextual_call({:get_greeting, name}, _from, state) do
    {:reply, {"hello #{name}", Process.get(:number, -1)}, state}
  end

  @impl true
  def handle_contextual_cast({:print_greeting, name}, state) do
    Logger.info(comment: "hello #{name}", number: Process.get(:number, 19))
    {:noreply, state}
  end
end
