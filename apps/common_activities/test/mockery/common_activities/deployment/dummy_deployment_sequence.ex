defmodule CommonActivities.Deployment.DummyDeploymentSequence do

  alias CommonActivities.Deployment.TestTopics

  use CommonActivities.Deployment.DeploymentSequence,
    get_deployment_reply_topic: TestTopics.get_deployment_reply,
    put_deployment_reply_topic: TestTopics.put_deployment_reply,
    supervisor: CommonActivities.TestTasker

    @impl true
    @spec get_app_key :: map
    def get_app_key, do: %{ app_name: "test-app", version: "1.0.0" }

    @impl true
    @spec get_app_manifest :: map
    def get_app_manifest, do: %{ app_key: get_app_key() }
end
