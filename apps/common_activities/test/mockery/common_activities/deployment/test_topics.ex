defmodule CommonActivities.Deployment.TestTopics do
  import Hermes.DynamicTopics

  @domain "org.leonardo.sak"

  @app_name :deployments

  @my_name :common_activities_deployment_test

  @get_deployment_reply compute_reply_topic(@domain, @app_name, @my_name, :get)
  @spec get_deployment_reply :: String.t
  def get_deployment_reply, do: @get_deployment_reply

  @put_deployment_reply compute_reply_topic(@domain, @app_name, @my_name, :put)
  @spec put_deployment_reply :: String.t
  def put_deployment_reply, do: @put_deployment_reply

end
