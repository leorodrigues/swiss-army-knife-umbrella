defmodule Core.Application do
  use Application
  @moduledoc false

  alias Core.Framework.Bootstrap

  alias Core.System.Activities.BecomeReady
  alias Core.System.Activities.BecomeReadyInjection
  alias Core.System.Activities.DeploymentSequence
  alias Core.System.Activities.ReactToDeploymentRequired
  alias Core.System.Activities.ReactToDeploymentRequiredInjection

  alias Core.Interface.Outbound.ExternalCommands

  alias Core.State.IdentityRecords

  alias Core.UseCases.ComputeNewIdentity

  @impl true
  def start(_type, _args) do
    children = [
      {Task.Supervisor, name: Core.Interface.Inbound.ReplyTasker},

      {Task.Supervisor, name: Core.System.Activities.Tasker},

      {Task.Supervisor, name: Core.UseCases.Tasker},

      IdentityRecords,

      ReactToDeploymentRequired.make_child_spec(%ReactToDeploymentRequiredInjection{
        notify: &ExternalCommands.notify_info_message/2
      }),

      BecomeReady.make_child_spec(%BecomeReadyInjection{
        notify: &ExternalCommands.notify_info_message/1
      }),

      ComputeNewIdentity.make_child_spec(
        &IdentityRecords.insert/1),

      DeploymentSequence
    ]

    opts = [strategy: :one_for_one, name: Core.Supervisor]
    result = Supervisor.start_link(children, opts)

    Bootstrap.run()

    result
  end
end
