defmodule Core.Domain.Identity do

  alias Core.Domain.Clock

  defstruct [
    value: nil,
    created_at: nil
  ]

  @type t :: %__MODULE__{
    value: String.t | nil,
    created_at: non_neg_integer | nil
  }

  def new do
    %__MODULE__{
      value: UUID.uuid4(),
      created_at: Clock.timestamp()
    }
  end
end
