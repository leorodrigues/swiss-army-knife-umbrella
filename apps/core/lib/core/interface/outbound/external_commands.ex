defmodule Core.Interface.Outbound.ExternalCommands do
  alias Core.Domain.Clock

  alias Core.Interface.Topics

  alias Hermes.Piping

  def notify_info_message(message) do
    make_payload(message) |> publish(Topics.put_notification_command)
  end

  def notify_info_message(message, meta) do
    make_payload(message, meta) |> publish(Topics.put_notification_command)
  end

  defp publish(payload, command_topic) do
    Piping.submit_void_command(command_topic, payload)
  end

  defp make_payload(content) do
    %{
      level: :info,
      message: content,
      timestamp: Clock.timestamp()
    }
  end

  defp make_payload(content, meta) do
    %{
      level: :info,
      message: content,
      timestamp: Clock.timestamp(),
      meta: meta
    }
  end
end
