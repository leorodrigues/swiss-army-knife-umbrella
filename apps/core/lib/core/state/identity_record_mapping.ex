defmodule Core.State.IdentityRecordMapping do

  alias Core.Domain.Identity

  def identity_to_record(%Identity{value: v, created_at: c}), do: {v, c}

  def record_to_identity({v, c}), do: %Identity{value: v, created_at: c}

  def record_to_identity(value), do: value
end
