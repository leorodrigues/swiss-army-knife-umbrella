defmodule Core.State.IdentityRecords do
  use Scrolls.PermanentScroll, scroll_name: :identity_records

  alias Core.Domain.Identity

  import Core.State.IdentityRecordMapping

  def insert(%Identity{ }= identity) do
    execute_fn(&handle_insert/2, [identity])
    identity
  end

  def load_current_identity do
    execute_fn(&handle_load_identity/2, [:current]) |> record_to_identity()
  end

  defp handle_insert(table, identity) do
    :dets.insert(table, {:current, identity_to_record(identity)})
  end

  def handle_load_identity(table, key) do
    match = [{{:"$1", :"$2"}, make_query(key), [:"$2"]}]
    case :dets.select(table, match) do
      [ ] -> :not_found
      r -> List.first(r)
    end
  end

  defp make_query(key) do
    [{:=, :"$1", key}]
  end
end
