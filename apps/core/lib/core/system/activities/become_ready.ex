defmodule Core.System.Activities.BecomeReady do

  alias CommonActivities.Basic.ContextualServer

  alias Core.System.Activities.Tasker

  alias Core.System.Activities.BecomeReadyInjection

  use ContextualServer, supervisor: Tasker

  require Logger

  @self __MODULE__

  # Business interface ---------------------------------------------------------

  def invoke do
    ContextualServer.call(@self, :invoke)
  end

  # GenServer interface --------------------------------------------------------

  @spec make_child_spec(injection :: BecomeReadyInjection.t) :: {atom, keyword}
  def make_child_spec(%BecomeReadyInjection{ } = i) do
    {@self, init_args: [injection: i], name: @self}
  end

  def start_link(opts \\ [ ]) do
    args = Keyword.get(opts, :init_args, [ ])
    GenServer.start_link(@self, args, name: @self)
  end

  @impl true
  def init(args \\ [ ]) do
    {:ok, Enum.into(args, %{ })}
  end

  @impl true
  def handle_contextual_call(:invoke, _from, %{injection: i} = state) do
    with %BecomeReadyInjection{notify: f} <- i, do: f.("Core ready")
    {:reply, :ok, state}
  end
end
