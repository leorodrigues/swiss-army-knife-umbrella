defmodule Core.System.Activities.ReactToDeploymentRequired do

  alias CommonActivities.Basic.ContextualServer

  alias Core.System.Activities.Tasker

  alias Core.System.Activities.ReactToDeploymentRequiredInjection

  alias Core.UseCases.ComputeNewIdentity

  alias Core.Domain.Identity

  use ContextualServer, supervisor: Tasker

  @self __MODULE__

  def invoke do
    ContextualServer.call(@self, :invoke)
  end

  @spec make_child_spec(ReactToDeploymentRequiredInjection.t) :: {atom, keyword}
  def make_child_spec(%ReactToDeploymentRequiredInjection{ } = i) do
    {@self, init_args: [injection: i], name: @self}
  end

  def start_link(opts \\ [ ]) do
    args = Keyword.get(opts, :init_args, [ ])
    GenServer.start_link(@self, args, name: @self)
  end

  @impl true
  def init(args \\ [ ]) do
    {:ok, Enum.into(args, %{ })}
  end

  @impl true
  def handle_contextual_call(:invoke, _from, %{injection: i} = state) do
    ComputeNewIdentity.invoke() |> notify(i)
    {:reply, :ok, state}
  end

  defp notify(%Identity{value: v, created_at: t}, i) do
    with %ReactToDeploymentRequiredInjection{notify: f} <- i do
      f.("New identity generated.", %{
        identity: v,
        created_at: t,
        app_name: :core,
        app_version: "1.0.0"
      })
    end
  end
end
