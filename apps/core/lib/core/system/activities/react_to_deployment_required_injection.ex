defmodule Core.System.Activities.ReactToDeploymentRequiredInjection do
  defstruct [notify: nil]

  @type notify :: (String.t, map -> :ok) | nil

  @type t :: %__MODULE__{notify: notify}
end
