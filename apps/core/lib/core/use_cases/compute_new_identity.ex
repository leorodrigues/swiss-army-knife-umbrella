defmodule Core.UseCases.ComputeNewIdentity do

  alias CommonActivities.Basic.ContextualServer

  alias Core.UseCases.Tasker

  alias Core.Domain.Identity

  use ContextualServer, supervisor: Tasker

  @type insert_identity_record :: (Identity.t -> Identity.t)

  # Business interface ---------------------------------------------------------

  @spec invoke :: Identity.t
  def invoke do
    ContextualServer.call(__MODULE__, :invoke)
  end

  # GenSever interface ---------------------------------------------------------

  @spec make_child_spec(insert :: insert_identity_record) :: {atom, keyword}
  def make_child_spec(insert) do
    {__MODULE__, init_args: [insert_identity_record: insert]}
  end

  def start_link(opts \\ [ ]) do
    args = Keyword.get(opts, :init_args, [ ])
    GenServer.start_link(__MODULE__, args, name: __MODULE__)
  end

  def stop do
    GenServer.stop(__MODULE__)
  end

  @impl true
  def init(args \\ [ ]) do
    {:ok, Enum.into(args, %{ })}
  end

  @impl true
  def handle_contextual_call(:invoke, _from, state) do
    handle_invoke(state) |> reply_call(state)
  end

  # Private interface ----------------------------------------------------------

  defp handle_invoke(%{insert_identity_record: f}), do: Identity.new() |> f.()

  defp reply_call(result, state), do: {:reply, result, state}

end
