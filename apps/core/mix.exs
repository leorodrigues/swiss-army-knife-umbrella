defmodule Core.MixProject do
  use Mix.Project

  def project do
    [
      app: :core,
      version: "1.0.0-a",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.15",
      elixirc_paths: elixirc_paths(Mix.env()),
      test_paths: test_paths(),
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      aliases: aliases(),
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.html": :test,
        "coveralls.unit": :test,
        "coveralls.proto": :test,
        integration: :test,
        unit: :test,
        proto: :test,
        regression: :test
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Core.Application, []}
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support", "test/mockery", "prototyping"]
  defp elixirc_paths(:dev), do: ["lib", "test/mockery", "prototyping"]
  defp elixirc_paths(_), do: ["lib"]

  defp test_paths do
    ["test/cases/unit", "test/cases/integration", "test/cases/regression"]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:logger_file_backend, "~> 0.0.13"},
      {:church, git: "http://bitbucket.org/leorodrigues/erlang-church.git", tag: "2.0.1"},
      {:sak_resources, in_umbrella: true},
      {:sak_test_support, only: [:test], in_umbrella: true},
      {:scrolls, git: "http://bitbucket.org/leorodrigues/scrolls.git", tag: "1.1.9"},
      {:hermes, git: "http://bitbucket.org/leorodrigues/ex-hermes.git", tag: "3.0.3"},
      {:sak_messaging, in_umbrella: true},
      {:meck, "~> 0.9.2", only: [:test], runtime: false},
      {:dialyxir, "~> 1.3", only: [:dev], runtime: false},
      {:excoveralls, "~> 0.14.4", only: [:test]},
      {:uuid, "~> 1.1.8"},
      {:common_activities, in_umbrella: true}
    ]
  end

  defp aliases do
    [
      integration: ["test test/cases/integration"],
      unit: ["test test/cases/unit"],
      regression: ["test test/cases/regression"],
      proto: ["test --only prototype:true"],
      "coveralls.unit": ["coveralls.html test/cases/unit"],
      "coveralls.proto": ["coveralls.html --only prototype:true"]
    ]
  end
end
