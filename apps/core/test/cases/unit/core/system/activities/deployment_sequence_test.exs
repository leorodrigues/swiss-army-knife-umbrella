defmodule Core.System.Activities.DeploymentSequenceTest do
  use ExUnit.Case

  alias Core.System.Activities.ReactToDeploymentRequired
  alias Core.System.Activities.DeploymentSequence
  alias Core.System.Activities.BecomeReady

  alias Core.System.Activities.Tasker

  alias Hermes.Piping

  setup do
    Application.put_env(:notifications, :skip_self_deploy, false)

    Task.Supervisor.start_link(name: Tasker)

    :meck.new([BecomeReady, Piping, ReactToDeploymentRequired])
    on_exit(fn -> :meck.unload end)
  end

  describe "Notifications.System.Activities.DeploymentSequence.continue" do
    test "should react to a deployment required" do
      :meck.expect(Piping, :submit_command, fn _, _, _ -> :ok end)
      :meck.expect(ReactToDeploymentRequired, :invoke, fn -> :ok end)

      DeploymentSequence.continue(:not_found)

      :meck.wait(Piping, :submit_command, :_, 1000)
      :meck.wait(ReactToDeploymentRequired, :invoke, [ ], 1000)
    end

    test "should become ready if the deployment is marked as deployed" do
      :meck.expect(BecomeReady, :invoke, fn -> :ok end)

      DeploymentSequence.continue(%{phase: :deployed})

      :meck.wait(BecomeReady, :invoke, [ ], 1000)
    end
  end

  describe "Notifications.System.Activities.DeploymentSequence.finish" do
    test "should become ready if the deployment is marked as deployed" do
      :meck.expect(BecomeReady, :invoke, fn -> :ok end)

      DeploymentSequence.finish(%{phase: :deployed})

      :meck.wait(BecomeReady, :invoke, [ ], 1000)
    end
  end
end
