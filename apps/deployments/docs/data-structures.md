# DATA STRUCTURES

## Deployment tuples

 - 0: uuid - Deployment identification
 - 1: atom - Application name
 - 2: string - Application semantic version
 - 3: timestamp - Creation date
 - 4: timestamp - Fulfillment date
 - 5: atom - Phase - One of :staged, :deployed
 - 6: atom - Status - One of :enabled, :disabled

## Dependency tuples

 - 0: uuid - Dependency identification
 - 1: uuid - Deployment identification
 - 2: atom - Required application name
 - 3: string - Required application semantic version
 - 4: atom - Dependency status - One of: :missing, :provided

## Event tuples

 - 0: uuid - Event identification
 - 1: uuid - Deployment identification
 - 2: atom - Event name

## Command tuples

 - 0: uuid - Command identification
 - 1: uuid - Deployment identification
 - 3: atom - Command name
