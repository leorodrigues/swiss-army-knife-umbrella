
```elixir

# select second element from 3 element tuples where first element is 1
# [{{:"$1", :"$2", :_}, [{:==, :"$1", 1}], [:"$2"]}]

# select everything with 3 elements and make a resulting tuple with two elements
# :dets.select(:tab, [{{:"$1", :"$2", :"$3"}, [], [{{:"$1", :"$3"}}]}])

# select everuthing where first element is equal to 1
# :dets.select(:tab, [{{:"$1", :"$2", :"$3"}, [{:==, :"$1", 1}], [{{:"$1", :test, :"$3"}}]}])

# make a match from a function
# f = :ets.fun2ms(fn {a, _, c} when a == 1 -> {a, c} end)

```