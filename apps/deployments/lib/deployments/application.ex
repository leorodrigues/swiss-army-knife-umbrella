defmodule Deployments.Application do
  @moduledoc false

  use Application

  alias Deployments.Interface.Outbound.ExternalCommands
  alias SakMessaging.CommandContext

  alias Deployments.State.ManifestScroll
  alias Deployments.State.CommandContextRecords

  alias Deployments.Interface.Outbound.DefaultEventEmitter

  alias Deployments.Interface.CommandContextServer

  alias Deployments.Interface.CallbackTasker
  alias Deployments.Interface.CommandTasker
  alias Deployments.Interface.EventTasker

  alias Deployments.Interface.Inbound.VoidCommandListener
  alias Deployments.Interface.Inbound.PutDeploymentListener
  alias Deployments.Interface.Inbound.GetDeploymentListener
  alias Deployments.Interface.Inbound.EventListener
  alias Deployments.Interface.Inbound.MutationCommandListener

  alias Deployments.UseCases.Dependencies.TryApplyingDeployment
  alias Deployments.UseCases.Dependencies.TryFulfillingDeployment
  alias Deployments.UseCases.GetDeployment
  alias Deployments.UseCases.PutDeployment
  alias Deployments.UseCases.ReviewDeployment

  alias Deployments.System.Activities.BecomeReady
  alias Deployments.System.Activities.BecomeReadyInjection
  alias Deployments.System.Activities.DeploymentSequence

  alias Deployments.Framework.Bootstrap

  @impl true
  def start(_type, _args) do
    children = [
      {Task.Supervisor, name: CallbackTasker},

      {Task.Supervisor, name: CommandTasker},

      {Task.Supervisor, name: EventTasker},

      {Task.Supervisor, name: Deplouments.System.Activities.Tasker},

      {Task.Supervisor, name: Deployments.UseCases.Tasker},

      ManifestScroll,

      CommandContextRecords,

      BecomeReady.make_child_spec(%BecomeReadyInjection{
        notify: &ExternalCommands.notify_info_message/1
      }),

      DeploymentSequence,

      TryApplyingDeployment.make_child_spec(
        &ManifestScroll.list_all_by_phase/1,
        &DefaultEventEmitter.fire_deployment_confirmed/1,
        &DefaultEventEmitter.fire_deployment_application_failed/1,
        &ManifestScroll.update/1),

      TryFulfillingDeployment.make_child_spec(
        &ManifestScroll.list_all_app_keys_by_phase_and_status/2,
        &ManifestScroll.find_by_app_key/1,
        &DefaultEventEmitter.fire_deployment_confirmed/1,
        &ManifestScroll.update/1),

      ReviewDeployment.make_child_spec(
        &ManifestScroll.find_by_app_key/1),

      GetDeployment.make_child_spec(
        &ManifestScroll.find_by_app_key/1),

      PutDeployment.make_child_spec(
        &ManifestScroll.insert/1,
        &DefaultEventEmitter.fire_deployment_staged/1),

      CommandContext.make_child_spec(
        CommandContextServer,
        CommandContextRecords),

      VoidCommandListener,
      GetDeploymentListener,
      MutationCommandListener,

      {PutDeploymentListener, init_args: [
        context_server_name: CommandContextServer
      ]},

      EventListener
    ]

    result = Supervisor.start_link(children, [
      strategy: :one_for_one,
      name: Deployments.Supervisor
    ])

    Bootstrap.run()

    result
  end
end
