defmodule Deployments.Domain.AppKey do

  alias Deployments.Domain.AppKey

  import :church

  defstruct [:name, :version]

  @type t :: %__MODULE__{
    name: atom, version: String.t
  }

  @type collection :: [ ] | [t]

  @spec make_from_map(map :: map) :: AppKey.t | {:error, reason :: any}
  def make_from_map(%{ } = map) do
    chain_apply(map, [
      partial_left(&require_key/2, [:app_name]),
      partial_left(&require_key/2, [:app_version]),
      &convert_map_to_app_key/1
    ])
  end

  @spec to_map(app_key :: AppKey.t) :: map
  def to_map(%AppKey{ } = app_key) do
    %{app_name: app_key.name, app_version: app_key.version}
  end

  defp convert_map_to_app_key(map) do
    %AppKey{
      name: Map.get(map, :app_name),
      version: Map.get(map, :app_version)
    }
  end

  defp require_key(map, key) do
    if Map.has_key?(map, key) do
      map
    else
      {:break, {:error, {to_string(__MODULE__), :required_key, key}}}
    end
  end
end
