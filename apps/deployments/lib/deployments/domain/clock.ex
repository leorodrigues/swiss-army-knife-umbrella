defmodule Deployments.Domain.Clock do
  def timestamp, do: System.system_time(:microsecond)
end
