defmodule Deployments.Domain.Debug do

  @use_case_check_point_message "Use case check point reached."

  def use_case_check_point(name, details \\ [ ]) do
    details
      |> collect_detail(use_case_name: name)
      |> collect_detail(comment: @use_case_check_point_message)
  end

  def collect_detail(details, new_detail) when is_list(details) do
    [new_detail|details]
  end
end
