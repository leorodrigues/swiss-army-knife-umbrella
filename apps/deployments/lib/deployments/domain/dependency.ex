defmodule Deployments.Domain.Dependency do
  alias Deployments.Domain.AppKey

  alias Deployments.Domain.Dependency

  defstruct [
    app_key: nil,
    status: :pending
  ]

  @type dependency_status :: :pending | :provided

  @type t :: %__MODULE__{
    app_key: AppKey.t,
    status: dependency_status
  }

  def make_from_map(%{ } = map) do
    case AppKey.make_from_map(map) do
      {:error, reason} ->
        {:error, {to_string(__MODULE__), reason}}
      app_key ->
        %Dependency{app_key: app_key, status: Map.get(map, :status, :pending)}
    end
  end

  def to_map(%Dependency{ } = dep) do
    Map.put(AppKey.to_map(dep.app_key), :status, dep.status)
  end
end
