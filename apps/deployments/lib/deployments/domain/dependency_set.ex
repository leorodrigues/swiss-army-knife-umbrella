defmodule Deployments.Domain.DependencySet do
  alias Deployments.Domain.Dependency

  def review_pending(dependencies, app_keys) do
    fulfill(dependencies, app_keys, [ ])
  end

  defp fulfill([ ], _, result), do: Enum.reverse(result)

  defp fulfill([%Dependency{status: :pending} = d|r], app_keys, result) do
    if Enum.member?(app_keys, d.app_key) do
      fulfill(r, app_keys, [mark_provided(d)|result])
    else
      fulfill(r, app_keys, [d|result])
    end
  end

  defp fulfill([%Dependency{status: :provided} = d|r], app_keys, result) do
    fulfill(r, app_keys, [d|result])
  end

  defp mark_provided(dependency) do
    %Dependency{dependency|status: :provided}
  end
end
