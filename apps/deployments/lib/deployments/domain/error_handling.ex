defmodule Deployments.Domain.ErrorHandling do

  require Logger

  def handle(%ArgumentError{message: m}, token), do: handle(m, token)

  def handle(%RuntimeError{message: m}, token), do: handle(m, token)

  def handle({:error, reason}, token), do: handle(reason, token)

  def handle(cause, token), do: {:error, {token, cause}}

  def log({:error, reason}, message, details \\ [ ]) do
    details
      |> collect_detail(error_chain: chain_errors(reason))
      |> collect_detail(comment: message)
      |> Logger.error()
  end

  def collect_detail(details, more_details) when is_list(details) do
    [more_details|details]
  end

  def chain_errors({current, cause}) do
    [current|chain_errors(cause)]
  end

  def chain_errors(cause), do: [cause]
end
