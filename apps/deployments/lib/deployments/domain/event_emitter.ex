defmodule Deployments.Domain.EventEmitter do
  alias Deployments.Domain.Manifest

  @callback fire_deployment_staged(manifest :: Manifest.t) :: :ok
  @callback fire_deployment_confirmed(manifest :: Manifest.t) :: :ok
  @callback fire_deployment_application_failed(error :: term) :: :ok
end
