defmodule Deployments.Domain.Manifest do

  alias Deployments.Domain.DependencySet
  alias Deployments.Domain.Dependency
  alias Deployments.Domain.Manifest
  alias Deployments.Domain.AppKey
  alias Deployments.Domain.Types
  alias Deployments.Domain.Clock

  import :church

  defstruct [
    id: nil,
    app_key: nil,
    phase: :staged,
    status: :enabled,
    dependencies: [],
    events: [],
    commands: [],
    created_at: nil,
    deployed_at: nil
  ]

  @type t :: %__MODULE__{
    id: String.t | nil,
    app_key: AppKey.t,
    phase: Types.deployment_phase,
    status: Types.deployment_status,
    dependencies: [Dependency.t],
    events: [String.t],
    commands: [String.t],
    created_at: DateTime.t | nil,
    deployed_at: DateTime.t | nil
  }

  @type collection :: [t]

  @spec review_pending_dependencies(
    manifest :: Manifest.t,
    app_keys :: [AppKey.t]
  ) :: Manifest.t
  def review_pending_dependencies(manifest, app_keys) do
    with %Manifest{dependencies: d} = manifest do
      chain_apply(d, [
        partial_left(&review_pending_deps/2, [app_keys]),
        partial_left(&replace_dependencies/2, [manifest]),
        &compute_new_phase/1
      ])
    end
  end

  @spec make_from_map(map :: map) :: Manifest.t | {:error, reason :: any}
  def make_from_map(%{ } = map) do
    chain_apply(map, [
      partial_left(&require_key/2, [:app_key]),
      &convert_map_to_manifest/1
    ])
  end

  @spec to_map(manifest :: Manifest.t) :: map
  def to_map(%Manifest{ } = manifest) do
    %{
      id: manifest.id,
      app_key: AppKey.to_map(manifest.app_key),
      phase: manifest.phase,
      status: manifest.status,
      dependencies: Enum.map(manifest.dependencies, &dep_to_map/1),
      events: manifest.events,
      commands: manifest.commands,
      created_at: manifest.created_at,
      deployed_at: manifest.deployed_at
    }
  end

  defp dep_to_map(dependency), do: Dependency.to_map(dependency)

  defp convert_map_to_manifest(map) do
    chain_apply(map, [
      &try_making_manifest_with_key/1,
      &break_on_error/1,
      partial_left(&stamp_creation_time/2, [map]),
      partial_left(&copy_field/3, [map, :id]),
      partial_left(&copy_field/3, [map, :phase]),
      partial_left(&copy_field/3, [map, :status]),
      partial_left(&convert_dependencies/2, [map]),
      &break_on_error/1,
      partial_left(&copy_field/3, [map, :events]),
      partial_left(&copy_field/3, [map, :commands]),
      partial_left(&copy_field/3, [map, :deployed_at])
    ])
  end

  defp try_making_manifest_with_key(map) do
    case AppKey.make_from_map(Map.get(map, :app_key)) do
      {:error, reason} -> {:error, {to_string(__MODULE__), reason}}
      k -> %Manifest{app_key: k}
    end
  end

  defp stamp_creation_time(manifest, map) do
    case Map.get(map, :created_at) do
      nil -> %Manifest{manifest|created_at: Clock.timestamp()}
      timestamp -> %Manifest{manifest|created_at: timestamp}
    end
  end

  defp convert_dependencies(%Manifest{ } = m, %{dependencies: deps}) do
    case convert_dependencies(deps, [ ]) do
      {:error, reason} -> {:error, {to_string(__MODULE__), reason}}
      converted_deps -> %Manifest{m|dependencies: converted_deps}
    end
  end

  defp convert_dependencies(%Manifest{ } = manifest, %{ }), do: manifest

  defp convert_dependencies([ ], result), do: Enum.reverse(result)
  defp convert_dependencies([h|tail], result) do
    case Dependency.make_from_map(h) do
      {:error, _} = e -> e
      dep -> convert_dependencies(tail, [dep|result])
    end
  end

  defp review_pending_deps(dependencies, app_keys) do
    DependencySet.review_pending(dependencies, app_keys)
  end

  defp replace_dependencies(dependencies, manifest) do
    %Manifest{manifest|dependencies: dependencies}
  end

  defp compute_new_phase(%Manifest{dependencies: d} = m) do
    case Enum.frequencies_by(d, fn %Dependency{status: s} -> s end) do
      %{pending: _} -> %Manifest{m|phase: :staged, deployed_at: nil}
      _ -> %Manifest{m|phase: :deployed, deployed_at: Clock.timestamp()}
    end
  end

  defp copy_field(manifest, map, key) do
    case Map.get(map, key) do
      nil -> manifest
      value -> Map.put(manifest, key, value)
    end
  end

  defp require_key(map, key) do
    if Map.has_key?(map, key) do
      map
    else
      {:break, {:error, {to_string(__MODULE__), :required_key, key}}}
    end
  end
end
