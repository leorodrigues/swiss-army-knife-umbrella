defmodule Deployments.Domain.ManifestRecords do

  alias Deployments.Domain.Manifest
  alias Deployments.Domain.AppKey
  alias Deployments.Domain.Types

  @callback insert(manifest :: Manifest.t) :: Manifest.t | Types.error

  @callback update(manifest :: Manifest.t) :: :ok | Types.error

  @callback remove(manifest :: Manifest.t) :: :ok | Types.error

  @callback find_by_app_key(
    app_key :: AppKey.t
  ) :: Manifest.t | :not_found | Types.error

  @callback list_all_by_phase(
    phase :: Types.deployment_phase
  ) :: [Manifest.t] | Types.error

  @callback list_all_app_keys_by_phase_and_status(
    phase :: Types.deployment_phase,
    status :: Types.deployment_status
  ) :: [AppKey.t] | Types.error
end
