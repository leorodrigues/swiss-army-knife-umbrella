defmodule Deployments.Domain.Types do
  alias Deployments.Domain.Manifest

  @type app_name :: atom

  @type app_version :: String.t

  @type app_key :: {app_name, app_version}

  @type deployment_phase :: :staged | :deployed

  @type deployment_status :: :enabled | :disabled

  @type manifest_action :: (Manifest.t -> :ok | error)

  @type error :: {:error, term}
end
