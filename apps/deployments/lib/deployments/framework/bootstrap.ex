defmodule Deployments.Framework.Bootstrap do

  alias Deployments.System.Activities.DeploymentSequence

  require Logger

  @app_name :deployments

  @comment "Boot strapping"

  @spec run :: :ok
  def run do
    Logger.debug([comment: @comment, app: @app_name])
    unless Application.get_env(@app_name, :skip_self_deploy, false) do
      DeploymentSequence.begin()
    end
    :ok
  end

end
