defmodule Deployments.Interface.Inbound.EventListener do
  use Hermes.Piping.Listener,
    supervisor: Deployments.Interface.EventTasker,
    pipeline: Hermes.Piping.Inbound.EventPipeline,
    inbox: "deployments"

  import :church

  import Deployments.Interface.PayloadMapping

  alias Deployments.Interface.Outbound.ExternalCommands

  alias Deployments.Interface.Topics

  require Logger

  listen_to Topics.deployment_staged_event, payload do
    chain_apply(payload, [
      &to_manifest/1,
      &break_on_error/1,
      &notify_phase_changed/1,
      &break_on_error/1,
      &to_command_response/1,
      &re_route_mutation/1
    ])
  end

  listen_to Topics.deployment_confirmed_event, payload do
    chain_apply(payload, [
      &to_manifest/1,
      &break_on_error/1,
      &notify_phase_changed/1,
      &break_on_error/1,
      &to_command_response/1,
      &re_route_mutation/1
    ])
  end

  listen_to Topics.deployment_application_failed_event, payload do
    notify_deployment_error(payload)
  end

  defp notify_phase_changed(manifest) do
    ExternalCommands.notify_deployment_phase_changed(manifest); manifest
  end

  defp notify_deployment_error(error) do
    ExternalCommands.notify_deployment_error(error)
  end

  defp re_route_mutation(payload) do
    Hermes.Piping.submit_void_command(Topics.react_to_mutation, payload)
  end
end
