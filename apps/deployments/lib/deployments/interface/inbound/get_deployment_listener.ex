defmodule Deployments.Interface.Inbound.GetDeploymentListener do
  use Hermes.Piping.Listener,
    supervisor: Deployments.Interface.CommandTasker,
    pipeline: Hermes.Piping.Inbound.CommandPipeline,
    inbox: "deployments"

  import :church

  import Deployments.Interface.PayloadMapping

  alias Deployments.Interface.Topics

  alias Deployments.UseCases.GetDeployment

  require Logger

  listen_to Topics.get_deployment_command, payload do
    chain_apply(payload, [
      &to_app_key/1,
      &break_on_error/1,
      &GetDeployment.invoke/1,
      &break_on_error/1,
      &to_command_response/1
    ])
  end
end
