defmodule Deployments.Interface.Inbound.MutationCommandListener do
  use Hermes.Piping.Listener,
    supervisor: Deployments.Interface.CommandTasker,
    pipeline: Hermes.Piping.Inbound.VoidCommandPipeline,
    inbox: "deployments"

  import :church

  import Deployments.Interface.PayloadMapping

  alias Deployments.Domain.Manifest

  alias Deployments.UseCases.Dependencies.TryFulfillingDeployment
  alias Deployments.UseCases.Dependencies.TryApplyingDeployment

  alias Deployments.Interface.Outbound.DelayedManifestReplyPipeline
  alias Deployments.Interface.Topics

  listen_to Topics.react_to_mutation, %{phase: :staged} = payload do
    chain_apply(payload, [
      &to_manifest/1,
      &break_on_error/1,
      &invoke_command_reply/1,
      &break_on_error/1,
      &TryFulfillingDeployment.invoke/1
    ])
  end

  listen_to Topics.react_to_mutation, %{phase: :deployed} = payload do
    chain_apply(payload, [
      &to_manifest/1,
      &break_on_error/1,
      partial_left(&invoke_command_reply/2, [true]),
      &break_on_error/1,
      &TryApplyingDeployment.invoke/1
    ])
  end

  listen_to Topics.react_to_mutation, p do
    Logger.warning(comment: "Unknown mutation", payload: p)
  end

  defp invoke_command_reply(%Manifest{ } = manifest, drop_context \\ false) do
    options = [drop_context: drop_context]
    DelayedManifestReplyPipeline.convey(manifest, options)
    manifest
  end
end
