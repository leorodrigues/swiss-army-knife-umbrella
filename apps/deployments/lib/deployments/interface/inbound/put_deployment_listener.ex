defmodule Deployments.Interface.Inbound.PutDeploymentListener do
  use Hermes.Piping.Listener,
    supervisor: Deployments.Interface.CommandTasker,
    pipeline: SakMessaging.LateReplyCommandPipeline,
    inbox: "deployments"

  import :church

  import Deployments.Interface.PayloadMapping

  alias Deployments.Interface.Topics

  alias Deployments.UseCases.PutDeployment

  require Logger

  listen_to Topics.put_deployment_command, payload do
    chain_apply(payload, [
      &to_manifest/1,
      &break_on_error/1,
      &PutDeployment.invoke/1,
      &break_on_error/1,
      &to_command_response/1
    ]) |> finish()
  end

  defp finish(%{id: id}), do: id
end
