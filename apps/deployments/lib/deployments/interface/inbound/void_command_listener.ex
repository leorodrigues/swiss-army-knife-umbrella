defmodule Deployments.Interface.Inbound.VoidCommandListener do
  use Hermes.Piping.Listener,
    supervisor: Deployments.Interface.CommandTasker,
    pipeline: Hermes.Piping.Inbound.VoidCommandPipeline,
    inbox: "deployments"

  import :church

  import Deployments.Interface.PayloadMapping

  alias Deployments.UseCases.ReviewDeployment

  alias Deployments.Interface.Topics

  listen_to Topics.review_deployment_command, payload do
    chain_apply(payload, [
      &to_app_key/1,
      &break_on_error/1,
      &ReviewDeployment.invoke/1
    ])
  end
end
