defmodule Deployments.Interface.Outbound.DefaultEventEmitter do

  alias Deployments.Domain.EventEmitter

  alias Deployments.Interface.Topics

  alias Hermes.Piping

  import Deployments.Interface.PayloadMapping

  require Logger

  @behaviour EventEmitter

  @impl EventEmitter
  def fire_deployment_staged(manifest) do
    Logger.debug(comment: "Firing deployment staged event", manifest: manifest)
    to_command_response(manifest) |> publish(Topics.deployment_staged_event)
  end

  @impl EventEmitter
  def fire_deployment_confirmed(manifest) do
    Logger.debug(comment: "Firing deployment confirmed event", manifest: manifest)
    to_command_response(manifest) |> publish(Topics.deployment_confirmed_event)
  end

  @impl EventEmitter
  def fire_deployment_application_failed(error) do
    Logger.debug(commnet: "Firing deployment application failed event")
    to_deployment_application_failed_event(error)
      |> publish(Topics.deployment_application_failed_event)
  end

  defp publish(payload, topic) do
    Piping.trigger_event(topic, payload)
    :ok
  end
end
