defmodule Deployments.Interface.Outbound.DelayedManifestReplyPipeline do
  use Hermes.Piping.Pipe

  alias Deployments.Domain.Manifest

  alias Deployments.Interface.CommandContextServer

  alias SakMessaging.CommandContext

  import Deployments.Interface.PayloadMapping

  pipe :restore_context
  pipe :parse_payload
  pipe Hermes.Piping.Outbound.MakeCommandReplyPipe
  pipe Hermes.Piping.Outbound.PublishReplyPipe

  defp restore_context(%Manifest{id: id}, true) do
    CommandContext.load(CommandContextServer, id)
    CommandContext.drop(CommandContextServer, id)
  end

  defp restore_context(%Manifest{id: id}, false) do
    CommandContext.load(CommandContextServer, id)
  end

  defp restore_context(manifest, options) do
    restore_context(manifest, Keyword.get(options, :drop_context, false))
    manifest
  end

  defp parse_payload(manifest, _), do: to_command_response(manifest)
end
