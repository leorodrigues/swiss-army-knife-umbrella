defmodule Deployments.Interface.Outbound.ExternalCommands do

  alias Deployments.Domain.ErrorHandling
  alias Deployments.Domain.Clock
  alias Deployments.Domain.AppKey
  alias Deployments.Domain.Manifest
  alias Deployments.Interface.Topics

  alias Hermes.Piping

  @app_staged_token "deployment-staged"

  @app_deployed_token "deployment-confirmed"

  @spec notify_deployment_phase_changed(manifest :: Manifest.t) :: :ok
  def notify_deployment_phase_changed(%Manifest{ } = manifest) do
    %{level: :info}
      |> add_manifest(manifest)
      |> publish(Topics.put_notification_command)
  end

  @spec notify_deployment_error(error :: any) :: :ok
  def notify_deployment_error(error) do
    %{level: :error}
      |> add_error(error, "Deployment error", :deployment_error)
      |> publish(Topics.put_notification_command)
  end

  @spec notify_info_message(message :: String.t) :: :ok
  def notify_info_message(message) do
    make_info_notification(message) |> publish(Topics.put_notification_command)
  end

  defp make_info_notification(message) do
    %{level: :info, message: message, timestamp: Clock.timestamp()}
  end

  defp add_error(map, error, message, token) do
    Map.put(map, :token, token)
      |> Map.put(:message, message)
      |> Map.put(:timestamp, Clock.timestamp())
      |> Map.put(:meta, make_meta(error))
  end

  defp add_manifest(map, payload) do
    case payload do
      %Manifest{phase: :staged, created_at: t, app_key: k} ->
        Map.put(map, :token, @app_staged_token)
          |> Map.put(:message, "Deployment staged")
          |> Map.put(:timestamp, t)
          |> Map.put(:meta, make_meta(k))

      %Manifest{phase: :deployed, deployed_at: t, app_key: k} ->
        Map.put(map, :token, @app_deployed_token)
          |> Map.put(:message, "Deployment confirmed")
          |> Map.put(:timestamp, t)
          |> Map.put(:meta, make_meta(k))
    end
  end

  defp publish(map, topic), do: Piping.submit_void_command(topic, map)

  defp make_meta(%AppKey{name: n, version: v}) do
    %{app_name: n, app_version: v}
  end

  defp make_meta({:error, _} = e) do
    %{error_sequence: ErrorHandling.chain_errors(e)}
  end
end
