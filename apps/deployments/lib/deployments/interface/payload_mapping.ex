defmodule Deployments.Interface.PayloadMapping do
  alias Deployments.Domain.ErrorHandling
  alias Deployments.Domain.Manifest
  alias Deployments.Domain.AppKey

  @spec to_app_key(map :: map) :: AppKey.t | {:error, reason :: any}
  def to_app_key(%{ } = map) do
    AppKey.make_from_map(map)
  end

  @spec to_manifest(map :: map) :: Manifest.t | {:error, reason :: any}
  def to_manifest(%{ } = map) do
    Manifest.make_from_map(map)
  end

  @spec to_command_response(
    manifest :: Manifest.t | :not_found
  ) :: map | :not_found
  def to_command_response(%Manifest{ } = m), do: Manifest.to_map(m)
  def to_command_response(:not_found), do: :not_found

  @spec to_deployment_application_failed_event(error :: {atom, term}) :: term
  def to_deployment_application_failed_event({:error, _} = e) do
    %{error_chain: ErrorHandling.chain_errors(e)}
  end

end
