defmodule Deployments.Interface.Topics do

  import Hermes.DynamicTopics

  @app_name :deployments
  @domain "org.leonardo.sak"

  @put_notification_command compute_command_topic(@domain, :notifications, :put)
  @spec put_notification_command :: String.t
  def put_notification_command, do: @put_notification_command

  @review_deployment_command compute_command_topic(@domain, @app_name, :review)
  @spec review_deployment_command :: String.t
  def review_deployment_command, do: @review_deployment_command

  @get_deployment_command compute_command_topic(@domain, @app_name, :get)
  @spec get_deployment_command :: String.t
  def get_deployment_command, do: @get_deployment_command

  @put_deployment_command compute_command_topic(@domain, @app_name, :put)
  @spec put_deployment_command :: String.t
  def put_deployment_command, do: @put_deployment_command

  @get_deployment_reply compute_reply_topic(@domain, @app_name, @app_name, :get)
  @spec get_deployment_reply :: String.t
  def get_deployment_reply, do: @get_deployment_reply

  @put_deployment_reply compute_reply_topic(@domain, @app_name, @app_name, :put)
  @spec put_deployment_reply :: String.t
  def put_deployment_reply, do: @put_deployment_reply

  @deployment_staged_event compute_event_topic(@domain, @app_name, :deployment_staged)
  @spec deployment_staged_event :: String.t
  def deployment_staged_event, do: @deployment_staged_event

  @deployment_confirmed_event compute_event_topic(@domain, @app_name, :deployment_confirmed)
  @spec deployment_confirmed_event :: String.t
  def deployment_confirmed_event, do: @deployment_confirmed_event

  @deployment_application_failed_event compute_event_topic(@domain, @app_name, :deployment_application_failed)
  @spec deployment_application_failed_event :: String.t
  def deployment_application_failed_event, do: @deployment_application_failed_event

  @react_to_mutation compute_command_topic(@domain, @app_name, :react_to_mutation)
  @spec react_to_mutation :: String.t
  def react_to_mutation, do: @react_to_mutation
end
