defmodule Deployments.State.CommandContextRecords do
  @behaviour SakMessaging.CommandContextRecords

  alias Deployments.Domain.ErrorHandling

  use Scrolls.PermanentScroll, scroll_name: :command_context_records

  @impl true
  def insert(key, context) do
    try do
      execute_fn(&handle_insert/3, [key, context])
    rescue
      e -> ErrorHandling.handle(e, :command_context_records_insert_failed)
    end
  end

  @impl true
  def remove_by_key(key) do
    try do
      execute_fn(&handle_remove_by_key/2, [key])
    rescue
      e -> ErrorHandling.handle(e, :command_context_records_delete_failed)
    end
  end

  @impl true
  def find_by_key(key) do
    try do
      execute_fn(&handle_find_by_key/2, [key])
    rescue
      e -> ErrorHandling.handle(e, :command_context_records_query_failed)
    end
  end

  defp handle_insert(table, key, context) do
    case :dets.insert(table, {key, context}) do
      {:error, _} = e -> ErrorHandling.handle(e, :command_context_records_insert_failed)
      :ok -> key
    end
  end

  defp handle_find_by_key(table, key) do
    match = [{{:"$1", :"$2"}, make_query(key), [:"$2"]}]
    case :dets.select(table, match) do
      [ ] -> :not_found
      {:error, _} = e -> ErrorHandling.handle(e, :command_context_records_query_failed)
      r -> List.first(r)
    end
  end

  defp handle_remove_by_key(table, key) do
    match = [{{:"$1", :_}, make_query(key), [true]}]
    case :dets.select_delete(table, match) do
      {:error, _} = e -> ErrorHandling.handle(e, :command_context_records_delete_failed)
      _ -> key
    end
  end

  defp make_query(key) do
    [{:==, :"$1", key}]
  end
end
