defmodule Deployments.State.DependencySetMapping do

  alias Deployments.Domain.Dependency

  import Enum, only: [reverse: 1]

  def dependency_set_to_record_set(dependencies) do
    deps_to_records(dependencies, [ ])
  end

  def record_set_to_dependency_set(records) do
    records_to_deps(records, [ ])
  end

  defp deps_to_records([ ], result), do: reverse(result)
  defp deps_to_records([%Dependency{app_key: k, status: s}|t], result) do
    deps_to_records(t, [{k, s}|result])
  end

  defp records_to_deps([ ], result), do: reverse(result)
  defp records_to_deps([{app_key, status}|t], result) do
    records_to_deps(t, [%Dependency{app_key: app_key, status: status}|result])
  end
end
