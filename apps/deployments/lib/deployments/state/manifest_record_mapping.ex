defmodule Deployments.State.ManifestRecordMapping do

  alias Deployments.Domain.Manifest

  import Deployments.State.DependencySetMapping

  def manifest_to_record(%Manifest{ } = m) do
    {
      m.id,
      m.app_key,
      dependency_set_to_record_set(m.dependencies),
      m.events,
      m.commands,
      m.phase,
      m.status,
      m.created_at,
      m.deployed_at
    }
  end

  def record_to_manifest({
    id, app_key, deps, events, commands, phase, status, created_at, deployed_at
  }) do
    %Manifest{
      id: id,
      app_key: app_key,
      phase: phase,
      status: status,
      dependencies: record_set_to_dependency_set(deps),
      events: events,
      commands: commands,
      created_at: created_at,
      deployed_at: deployed_at
    }
  end
end
