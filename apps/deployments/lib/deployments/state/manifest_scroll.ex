defmodule Deployments.State.ManifestScroll do
  use Scrolls.PermanentScroll, scroll_name: :manifest_records

  alias Deployments.Domain.ErrorHandling
  alias Deployments.Domain.Manifest
  alias Deployments.Domain.ManifestRecords

  import Deployments.State.ManifestRecordMapping

  @behaviour ManifestRecords

  @app_key_only {:_, :"$1", :_, :_, :_, :"$2", :"$3", :_, :_}

  @impl ManifestRecords
  def insert(%Manifest{ } = m) do
    try do
      execute_fn(&handle_insert/2, [%Manifest{m|id: UUID.uuid4()}])
    rescue
      e -> ErrorHandling.handle(e, :manifest_scroll_insert_failed)
    end
  end

  @impl ManifestRecords
  def update(%Manifest{ } = m) do
    try do
      case execute_fn(&handle_insert/2, [m]) do
        {:error, _} = e -> e
        _ -> :ok
      end
    rescue
      e -> ErrorHandling.handle(e, :manifest_scroll_update_failed)
    end
  end

  @impl ManifestRecords
  def remove(%Manifest{id: id}) do
    try do
      execute_fn(&handle_remove/2, [id])
    rescue
      e -> ErrorHandling.handle(e, :manifest_scroll_remove_failed)
    end
  end

  @impl ManifestRecords
  def find_by_app_key(app_key) do
    try do
      execute_fn(&handle_find_by_app_key/2, [app_key])
    rescue
      e -> ErrorHandling.handle(e, :manifest_scroll_query_failed)
    end
  end

  @impl ManifestRecords
  def list_all_by_phase(phase) do
    try do
      execute_fn(&handle_list_all_by_phase/2, [phase])
    rescue
      e -> ErrorHandling.handle(e, :manifest_scroll_query_failed)
    end
  end

  @impl ManifestRecords
  def list_all_app_keys_by_phase_and_status(phase, status) do
    try do
      f = &handle_list_all_app_keys_by_phase_and_status/3
      execute_fn(f, [phase, status])
    rescue
      e -> ErrorHandling.handle(e, :manifest_scroll_query_failed)
    end
  end

  defp handle_insert(table, manifest) do
    case :dets.insert(table, manifest_to_record(manifest)) do
      :ok -> manifest
      {:error, _} = e -> ErrorHandling.handle(e, :manifest_scroll_insert_failed)
    end
  end

  defp handle_remove(table, key) do
    case :dets.delete(table, key) do
      {:error, _} = e -> ErrorHandling.handle(e, :manifest_scroll_remove_failed)
      anything_else -> anything_else
    end
  end

  defp handle_find_by_app_key(table, app_key) do
    match_spec = [{@app_key_only, [{:==, :"$1", app_key}], [:"$_"]}]
    case :dets.select(table, match_spec) do
      [ ] -> :not_found
      {:error, _} = e -> ErrorHandling.handle(e, :manifest_scroll_query_failed)
      results -> results |> List.first() |> record_to_manifest()
    end
  end

  defp handle_list_all_by_phase(table, phase)  do
    match_spec = [{@app_key_only, [{:==, :"$2", phase}], [:"$_"]}]
    case :dets.select(table, match_spec) do
      {:error, _} = e -> ErrorHandling.handle(e, :manifest_scroll_query_failed)
      results -> results |> Enum.map(&record_to_manifest/1) |> Enum.sort()
    end
  end

  defp handle_list_all_app_keys_by_phase_and_status(table, phase, status)  do
    match_spec = [{@app_key_only, make_query(phase, status), [:"$1"]}]
    case :dets.select(table, match_spec) do
      {:error, _} = e -> ErrorHandling.handle(e, :manifest_scroll_query_failed)
      results -> results |> Enum.sort()
    end
  end

  defp make_query(phase, status) do
    [{:andalso, {:==, :"$2", phase}, {:==, :"$3", status}}]
  end
end
