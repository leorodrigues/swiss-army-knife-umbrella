defmodule Deployments.UseCases.Dependencies.TryApplyingDeployment do

  alias CommonActivities.Basic.ContextualServer
  alias Deployments.UseCases.Tasker

  use ContextualServer, supervisor: Tasker

  alias Deployments.Domain.ErrorHandling
  alias Deployments.Domain.Manifest
  alias Deployments.Domain.Types

  import Deployments.Domain.Debug, only: [use_case_check_point: 2]

  require Logger

  @type find_manifest_by_phase :: (
    Types.deployment_phase -> Manifest.collection | Types.error
  )

  @use_case_name "try-applying-deployment"

  # Business interface ---------------------------------------------------------

  @spec invoke(manifest :: Manifest.t) :: [Manifest.t] | Types.error
  def invoke(%Manifest{app_key: app_key}) do
    ContextualServer.call(__MODULE__, {:invoke, app_key})
  end

  # GenSever interface ---------------------------------------------------------

  @spec make_child_spec(
    find_manifest_by_phase :: find_manifest_by_phase,
    fire_deployment_confirmed :: Types.manifest_action,
    fire_deployment_application_failed :: Types.manifest_action,
    update_manifest :: Types.manifest_action
  ) :: {atom, keyword}
  def make_child_spec(
    find_manifest_by_phase,
    fire_deployment_confirmed,
    fire_deployment_application_failed,
    update_manifest
  ) do
    {__MODULE__, init_args: [
      find_manifest_by_phase: find_manifest_by_phase,
      fire_deployment_confirmed: fire_deployment_confirmed,
      fire_deployment_application_failed: fire_deployment_application_failed,
      update_manifest: update_manifest
    ]}
  end

  def start_link(opts \\ [ ]) do
    args = Keyword.get(opts, :init_args, [ ])
    GenServer.start_link(__MODULE__, args, name: __MODULE__)
  end

  def stop do
    GenServer.stop(__MODULE__)
  end

  @impl true
  def init(args \\ [ ]) do
    {:ok, Enum.into(args, %{ })}
  end

  @impl true
  def handle_contextual_call({:invoke, app_key}, _from, state) do
    handle_invoke(app_key, state)
    {:reply, :ok, state}
  end

  # Private interface ----------------------------------------------------------

  defp handle_invoke(app_key, %{find_manifest_by_phase: f} = state) do
    Logger.debug(use_case_check_point(@use_case_name, app_key: app_key))
    f.(:staged) |> do_the_thing(app_key, state)
  end

  defp do_the_thing({:error, _} = e, _, _), do: e

  defp do_the_thing(manifests, app_key, state) do
    manifests
      |> Stream.map(resolve_dependencies(app_key))
      |> Stream.map(update_manifest(state))
      |> Stream.filter(&exclude_staged/1)
      |> Stream.each(fire_events(state))
      |> Stream.run()
  end

  defp resolve_dependencies(app_key) do
    fn m -> Manifest.review_pending_dependencies(m, [app_key]) end
  end

  defp update_manifest(%{update_manifest: u}) do
    fn m -> u.(m) |> manifest_or_error(m) end
  end

  defp manifest_or_error(:ok, m), do: m
  defp manifest_or_error(error, _), do: error

  defp exclude_staged(%Manifest{phase: :deployed}), do: true
  defp exclude_staged({:error, _}), do: true
  defp exclude_staged(_), do: false

  defp fire_events(%{fire_deployment_confirmed: f} = state) do
    with %{fire_deployment_application_failed: g} <- state do
      fn
        %Manifest{ } = m -> f.(m)
        {:error, _} = e -> e |> wrap_error() |> g.()
      end
    end
  end

  defp wrap_error(error) do
    ErrorHandling.handle(error, :try_applying_deployment_failed)
  end
end
