defmodule Deployments.UseCases.Dependencies.TryFulfillingDeployment do

  alias CommonActivities.Basic.ContextualServer
  alias Deployments.UseCases.Tasker

  use ContextualServer, supervisor: Tasker

  alias Deployments.Domain.AppKey
  alias Deployments.Domain.Manifest
  alias Deployments.Domain.Types

  import Deployments.Domain.Debug, only: [use_case_check_point: 2]
  import :church

  require Logger

  @type deployment_phase :: Types.deployment_phase

  @type deployment_status :: Types.deployment_status

  @type collection :: AppKey.collection

  @type error :: Types.error

  @type find_by_phase_and_status :: (
    deployment_phase, deployment_status -> collection | error
  )

  @type find_by_app_key :: (AppKey.t -> Manifest.t | error)

  @use_case_name "try-fulfilling-deployment"

  # Business interface ---------------------------------------------------------

  @spec invoke(manifest :: Manifest.t) :: Manifest.t | error
  def invoke(%Manifest{ } = manifest) do
    ContextualServer.call(__MODULE__, {:invoke, manifest})
  end

  # GenSever interface ---------------------------------------------------------

  @spec make_child_spec(
    find :: find_by_phase_and_status,
    find_by_app_key :: find_by_app_key,
    fire :: Types.manifest_action,
    update :: Types.manifest_action
  ) :: {atom, keyword}
  def make_child_spec(find, find_by_app_key, fire, update) do
    {__MODULE__, init_args: [
      find_by_app_key: find_by_app_key,
      list_all_app_keys_by_phase_and_status: find,
      fire_deployment_confirmed: fire,
      update_manifest: update
    ]}
  end

  def start_link(opts \\ [ ]) do
    args = Keyword.get(opts, :init_args, [ ])
    GenServer.start_link(__MODULE__, args, name: __MODULE__)
  end

  def stop do
    GenServer.stop(__MODULE__)
  end

  @impl true
  def init(args \\ [ ]) do
    {:ok, Enum.into(args, %{ })}
  end

  @impl true
  def handle_contextual_call({:invoke, manifest}, _from, state) do
    handle_invoke(manifest, state) |> reply_call()
  end

  # Private interface ----------------------------------------------------------

  defp handle_invoke(manifest, state) do
    Logger.debug(use_case_check_point(@use_case_name, manifest: manifest))
    try_to_fulfill(reload_manifest(manifest, state), state)
  end

  defp try_to_fulfill(%Manifest{phase: :deployed} = m, state) do
    Logger.debug(comment: "Manifest is already deployed.", manifest: m)
    {m, state}
  end

  defp try_to_fulfill(%Manifest{phase: :staged} = m, state) do
    chain_apply(get_app_keys(state), [
      partial_left(&resolve_dependencies/2, [m]),
      partial_left(&update_manifest/2, [state]),
      partial_left(&maybe_fire_event/2, [state])
    ])
  end

  defp reload_manifest(%Manifest{app_key: k}, %{find_by_app_key: f}), do: f.(k)

  defp resolve_dependencies(keys, manifest) do
    Manifest.review_pending_dependencies(manifest, keys)
  end

  defp update_manifest(m, %{update_manifest: f}) do
    f.(m); m
  end

  defp maybe_fire_event(%Manifest{phase: :staged} = m, state), do: {m, state}

  defp maybe_fire_event(%Manifest{phase: :deployed} = m, state) do
    with %{fire_deployment_confirmed: f} <- state, do: f.(m); {m, state}
  end

  defp get_app_keys(%{list_all_app_keys_by_phase_and_status: f}) do
    f.(:deployed, :enabled)
  end

  defp reply_call({result, state}), do: {:reply, result, state}
end
