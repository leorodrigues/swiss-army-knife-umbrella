defmodule Deployments.UseCases.GetDeployment do

  alias CommonActivities.Basic.ContextualServer
  alias Deployments.UseCases.Tasker

  use ContextualServer, supervisor: Tasker

  alias Deployments.Domain.ErrorHandling
  alias Deployments.Domain.Manifest
  alias Deployments.Domain.AppKey
  alias Deployments.Domain.Types

  import Deployments.Domain.Debug, only: [use_case_check_point: 2]

  require Logger

  @type find_manifest_by_app_key :: (
    AppKey.t -> Manifest.t | :not_found | Types.error
  )

  @use_case_name "get-deployment"

  # Business interface ---------------------------------------------------------

  @spec invoke(app_key :: AppKey.t) :: Manifest.t | :not_found | Types.error
  def invoke(app_key) do
    ContextualServer.call(__MODULE__, {:invoke, app_key})
  end

  # GenSever interface ---------------------------------------------------------

  @spec make_child_spec(find :: find_manifest_by_app_key) :: {atom, keyword}
  def make_child_spec(find) do
    {__MODULE__, init_args: [find_manifest_by_app_key: find]}
  end

  def start_link(opts \\ [ ]) do
    args = Keyword.get(opts, :init_args, [ ])
    GenServer.start_link(__MODULE__, args, name: __MODULE__)
  end

  def stop do
    GenServer.stop(__MODULE__)
  end

  @impl true
  def init(args \\ [ ]) do
    {:ok, Enum.into(args, %{ })}
  end

  @impl true
  def handle_contextual_call({:invoke, app_key}, _from, state) do
    Logger.debug(use_case_check_point(@use_case_name, app_key: app_key))
    handle_invoke(app_key, state) |> reply_call()
  end

  # Private interface ----------------------------------------------------------

  def handle_invoke(app_key, state) do
    with %{find_manifest_by_app_key: find} <- state do
      case find.(app_key) do
        {:error, _} = e -> ErrorHandling.handle(e, :get_deployment_failed)
        value -> {value, state}
      end
    end
  end

  defp reply_call({result, state}), do: {:reply, result, state}
end
