defmodule Deployments.UseCases.PutDeployment do

  alias CommonActivities.Basic.ContextualServer
  alias Deployments.UseCases.Tasker

  use ContextualServer, supervisor: Tasker

  alias Deployments.Domain.ErrorHandling
  alias Deployments.Domain.Manifest
  alias Deployments.Domain.Types

  import Deployments.Domain.Debug, only: [use_case_check_point: 2]

  require Logger

  @type insert_manifest :: (Manifest.t -> Manifest.t | Types.error)

  @use_case_name "put-deployment"

  # Business interface ---------------------------------------------------------

  @spec invoke(manifest :: Manifest.t) :: Manifest.t | Types.error
  def invoke(manifest) do
    ContextualServer.call(__MODULE__, {:invoke, manifest})
  end

  # GenSever interface ---------------------------------------------------------

  @spec make_child_spec(
    find :: insert_manifest,
    fire :: Types.manifest_action
  ) :: {atom, keyword}
  def make_child_spec(insert, fire) do
    {__MODULE__, init_args: [
      insert_manifest: insert,
      fire_deployment_staged: fire
    ]}
  end

  def start_link(opts \\ [ ]) do
    args = Keyword.get(opts, :init_args, [ ])
    GenServer.start_link(__MODULE__, args, name: __MODULE__)
  end

  def stop do
    GenServer.stop(__MODULE__)
  end

  @impl true
  def init(args \\ [ ]) do
    {:ok, Enum.into(args, %{ })}
  end

  @impl true
  def handle_contextual_call({:invoke, manifest}, _from, state) do
    Logger.debug(use_case_check_point(@use_case_name, manifest: manifest))
    handle_invoke(manifest, state) |> reply_call()
  end

  # Private interface ----------------------------------------------------------

  defp handle_invoke(m, state), do: save_manifest(m, state) |> fire_event(state)

  defp save_manifest(manifest, %{insert_manifest: f}), do: f.(manifest)

  defp fire_event(%Manifest{ } = m, state) do
    with %{fire_deployment_staged: fire_event} <- state do
      case fire_event.(m) do
        {:error, _} = e -> ErrorHandling.handle(e, :put_deployment_failed)
        _ -> {m, state}
      end
    end
  end

  defp reply_call({result, state}), do: {:reply, result, state}

end
