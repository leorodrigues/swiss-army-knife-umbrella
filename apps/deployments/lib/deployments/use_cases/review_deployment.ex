defmodule Deployments.UseCases.ReviewDeployment do

  alias CommonActivities.Basic.ContextualServer
  alias Deployments.UseCases.Tasker

  use ContextualServer, supervisor: Tasker

  alias Deployments.UseCases.Dependencies.TryFulfillingDeployment

  alias Deployments.Domain.AppKey
  alias Deployments.Domain.Manifest
  alias Deployments.Domain.ErrorHandling

  import Deployments.Domain.Debug, only: [use_case_check_point: 2]

  require Logger

  @type find_manifest_by_app_key :: (
    AppKey.t -> Manifest.t | :not_found | Types.error
  )

  @use_case_name "review-deployment"

  # Business interface ---------------------------------------------------------

  @spec invoke(app_key :: AppKey.t) :: :ok | Types.error
  def invoke(%AppKey{ } = app_key) do
    ContextualServer.call(__MODULE__, {:invoke, app_key})
  end

  # GenSever interface ---------------------------------------------------------

  @spec make_child_spec(find :: find_manifest_by_app_key) :: {atom, keyword}
  def make_child_spec(find) do
    {__MODULE__, init_args: [find_manifest_by_app_key: find]}
  end

  def start_link(opts \\ [ ]) do
    args = Keyword.get(opts, :init_args, [ ])
    GenServer.start_link(__MODULE__, args, name: __MODULE__)
  end

  def stop do
    GenServer.stop(__MODULE__)
  end

  @impl true
  def init(args \\ [ ]) do
    {:ok, Enum.into(args, %{ })}
  end

  @impl true
  def handle_contextual_call({:invoke, app_key}, _from, state) do
    Logger.debug(use_case_check_point(@use_case_name, app_key: app_key))
    handle_invoke(app_key, state) |> reply_call()
  end

  # Private interface ----------------------------------------------------------

  defp handle_invoke(app_key, %{find_manifest_by_app_key: find} = state) do
    case find.(app_key) do
      :not_found -> {:not_found, state}
      {:error, _} = e -> {wrap_error(e), state}
      manifest -> {try_fulfilling_deployment(manifest), state}
    end
  end

  defp try_fulfilling_deployment(manifest) do
    case TryFulfillingDeployment.invoke(manifest) do
      {:error, _} = e -> wrap_error(e)
      result -> result
    end
  end

  defp wrap_error(error) do
    ErrorHandling.handle(error, :review_deployment_failed)
  end

  defp reply_call({result, state}), do: {:reply, result, state}
end
