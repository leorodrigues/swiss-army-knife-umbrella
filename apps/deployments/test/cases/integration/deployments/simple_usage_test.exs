defmodule Deployments.SimpleUsageTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :deployments

  alias Deployments.Test.CommandTasker

  alias Deployments.Interface.Topics
  alias Deployments.EmptyHandlers

  alias Deployments.TestTopics

  alias Deployments.TestReplyListener
  alias Deployments.TestNotificationsListener

  alias Deployments.Interface.Inbound.SimpleUsageEventListener

  alias Hermes.Dispatching.Dispatcher

  import Deployments.TestUtils

  @inbox "deployment-tests"

  @test_listeners [
    TestReplyListener,
    TestNotificationsListener,
    SimpleUsageEventListener
  ]

  setup_all_environment do
    Hermes.set_dispatch_on_publish(:single_topic)
    Hermes.set_sleep_interval(5000)
    [ ]
  end

  setup ctx do
    :meck.new([EmptyHandlers, Dispatcher], [:passthrough])
    on_exit(fn -> :meck.unload end)

    Task.Supervisor.start_link(name: CommandTasker)

    case ctx do
      %{samples_file: f} -> load_samples(ctx, resolve_test_resource_path([f]))
      _ -> :ok
    end
  end

  describe "Deployments simple usage cases," do

    @tag samples_file: "case1.terms"
    test "simulation of a standard deployment negociation for one app", %{samples: samples} do

      with_listeners(@test_listeners, fn ->

        # try to get the deployment and see that it has not yet been deployed
        using_sample(samples, :get_command)
          |> submit_command(TestTopics.get_deployment_command_reply, Topics.get_deployment_command)

        Process.sleep(1000)

        using_sample(samples, :first_get_reply)
          |> wait_on(EmptyHandlers, :handle_get, TestTopics.get_deployment_command_reply, @inbox, 3000)


        # put the deployment
        using_sample(samples, :put_command)
          |> submit_command(TestTopics.put_deployment_command_reply, Topics.put_deployment_command)

        Process.sleep(1000)

        using_sample(samples, :deployment_staged_event)
          |> wait_on(EmptyHandlers, :handle_staged, Topics.deployment_staged_event, @inbox, 3000)
          |> ensure_call_count(1, EmptyHandlers, :handle_staged, Topics.deployment_staged_event, @inbox)

        using_sample(samples, :deployment_confirmed_event)
          |> wait_on(EmptyHandlers, :handle_confirmed, Topics.deployment_confirmed_event, @inbox, 3000)
          |> ensure_call_count(1, EmptyHandlers, :handle_confirmed, Topics.deployment_confirmed_event, @inbox)

        using_sample(samples, :first_put_reply)
          |> wait_on(EmptyHandlers, :handle_put, TestTopics.put_deployment_command_reply, @inbox, 3000)
          |> ensure_call_count(1, EmptyHandlers, :handle_put, TestTopics.put_deployment_command_reply, @inbox)

        using_sample(samples, :second_put_reply)
          |> wait_on(EmptyHandlers, :handle_put, TestTopics.put_deployment_command_reply, @inbox, 3000)
          |> ensure_call_count(1, EmptyHandlers, :handle_put, TestTopics.put_deployment_command_reply, @inbox)


        # try to get the deployment and see that it has been successfuly deployed
        using_sample(samples, :get_command)
          |> submit_command(TestTopics.get_deployment_command_reply, Topics.get_deployment_command)

        Process.sleep(1000)

        using_sample(samples, :second_get_reply)
          |> wait_on(EmptyHandlers, :handle_get, TestTopics.get_deployment_command_reply, @inbox, 3000)
          |> ensure_call_count(2, EmptyHandlers, :handle_get, TestTopics.get_deployment_command_reply, @inbox)
      end)
    end

    @tag samples_file: "case2.terms"
    test "simulation of a standard deployment negociation for two apps", %{samples: samples} do

      with_listeners(@test_listeners, fn ->

        # put the deployment
        using_sample(samples, :put_dependent_app_command)
          |> submit_command(TestTopics.put_deployment_command_reply, Topics.put_deployment_command)

        Process.sleep(1000)

        using_sample(samples, :dependent_app_staged_reply)
          |> wait_on(EmptyHandlers, :handle_put, TestTopics.put_deployment_command_reply, @inbox, 3000)


        using_sample(samples, :put_required_app_command)
          |> submit_command(TestTopics.put_deployment_command_reply, Topics.put_deployment_command)

        Process.sleep(1000)

        using_sample(samples, :required_app_staged_reply)
          |> wait_on(EmptyHandlers, :handle_put, TestTopics.put_deployment_command_reply, @inbox, 3000)

        using_sample(samples, :required_app_confirmed_reply)
          |> wait_on(EmptyHandlers, :handle_put, TestTopics.put_deployment_command_reply, @inbox, 3000)

        using_sample(samples, :dependent_app_confirmed_reply)
          |> wait_on(EmptyHandlers, :handle_put, TestTopics.put_deployment_command_reply, @inbox, 3000)
      end)
    end
  end
end
