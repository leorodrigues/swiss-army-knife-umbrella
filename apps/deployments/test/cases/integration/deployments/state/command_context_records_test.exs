defmodule Deployments.State.CommandContextRecordsTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :deployments

  alias Deployments.State.CommandContextRecords

  setup_all_environment do
    [ ]
  end

  setup do
    on_exit(fn -> :meck.unload() end)
  end

  describe "Deployments.State.CommandContextRecords" do
    test "should save/retrieve/remove elements correctly" do
      key = "9b177a30-20ee-4720-bb95-2114e4d333e6"

      ^key = CommandContextRecords.insert(key, :fake_context)

      :fake_context = CommandContextRecords.find_by_key(key)

      ^key = CommandContextRecords.remove_by_key(key)

      :not_found = CommandContextRecords.find_by_key(key)
    end
  end
end
