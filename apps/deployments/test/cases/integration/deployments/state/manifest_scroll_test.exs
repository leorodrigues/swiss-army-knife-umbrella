defmodule Deployments.State.ManifestScrollTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :deployments

  alias Deployments.Domain.AppKey
  alias Deployments.Domain.Manifest
  alias Deployments.State.ManifestScroll

  setup_all_environment do
    [ ]
  end

  setup do
    ManifestScroll.list_all_by_phase(:deployed)
      |> Enum.each(&ManifestScroll.remove/1)

    ManifestScroll.list_all_by_phase(:staged)
      |> Enum.each(&ManifestScroll.remove/1)

    :meck.new(UUID, [:passthrough])
    on_exit(fn -> :meck.unload() end)
  end

  describe "Deployments.State.ManifestScroll" do
    test "should save and retrieve a manifest" do
      app_key = %AppKey{name: :some_app, version: "1.0"}

      manifest = %Manifest{
        app_key: app_key,
        phase: :staged,
        status: :enabled,
        dependencies: [],
        commands: [],
        events: [],
        created_at: 1697244110540698000,
        deployed_at: 1697244110540698000
      }

      expected_manifest = %Manifest{
        id: "1234abcd",
        app_key: app_key,
        phase: :staged,
        status: :enabled,
        dependencies: [],
        commands: [],
        events: [],
        created_at: 1697244110540698000,
        deployed_at: 1697244110540698000
      }

      :meck.expect(UUID, :uuid4, fn -> "1234abcd" end)

      ^expected_manifest = ManifestScroll.insert(manifest)

      ^expected_manifest = ManifestScroll.find_by_app_key(app_key)

      ManifestScroll.remove(expected_manifest)

      :not_found = ManifestScroll.find_by_app_key(app_key)
    end


    test "should list all app keys with a given phase and status" do
      app_key_a = %AppKey{name: :app_a, version: "1.0"}
      app_key_b = %AppKey{name: :app_b, version: "1.0"}
      app_key_c = %AppKey{name: :app_c, version: "1.0"}
      app_key_d = %AppKey{name: :app_d, version: "1.0"}

      m1 = ManifestScroll.insert(%Manifest{
        app_key: app_key_a,
        phase: :staged,
        status: :enabled,
        dependencies: [],
        commands: [],
        events: [],
        created_at: 1697244110540698000,
        deployed_at: 1697244110540698000
      })

      m2 = ManifestScroll.insert(%Manifest{
        app_key: app_key_b,
        phase: :staged,
        status: :disabled,
        dependencies: [],
        commands: [],
        events: [],
        created_at: 1697244110540698000,
        deployed_at: 1697244110540698000
      })

      m3 = ManifestScroll.insert(%Manifest{
        app_key: app_key_c,
        phase: :deployed,
        status: :enabled,
        dependencies: [],
        commands: [],
        events: [],
        created_at: 1697244110540698000,
        deployed_at: 1697244110540698000
      })

      m4 = ManifestScroll.insert(%Manifest{
        app_key: app_key_d,
        phase: :deployed,
        status: :disabled,
        dependencies: [],
        commands: [],
        events: [],
        created_at: 1697244110540698000,
        deployed_at: 1697244110540698000
      })

      [^app_key_a] = ManifestScroll.list_all_app_keys_by_phase_and_status(:staged, :enabled)
      [^app_key_d] = ManifestScroll.list_all_app_keys_by_phase_and_status(:deployed, :disabled)

      Enum.each([m1,m2,m3,m4], fn m -> ManifestScroll.remove(m) end)
    end

    test "should list all in a given phase" do
      app_key_a = %AppKey{name: :app_1, version: "1.0"}
      app_key_b = %AppKey{name: :app_2, version: "1.0"}
      app_key_c = %AppKey{name: :app_3, version: "1.0"}
      app_key_d = %AppKey{name: :app_4, version: "1.0"}

      m1 = ManifestScroll.insert(%Manifest{
        app_key: app_key_a,
        phase: :staged,
        status: :enabled,
        dependencies: [],
        commands: [],
        events: [],
        created_at: 1697244110540698000,
        deployed_at: 1697244110540698000
      })

      m2 = ManifestScroll.insert(%Manifest{
        app_key: app_key_b,
        phase: :staged,
        status: :disabled,
        dependencies: [],
        commands: [],
        events: [],
        created_at: 1697244110540698000,
        deployed_at: 1697244110540698000
      })

      m3 = ManifestScroll.insert(%Manifest{
        app_key: app_key_c,
        phase: :deployed,
        status: :enabled,
        dependencies: [],
        commands: [],
        events: [],
        created_at: 1697244110540698000,
        deployed_at: 1697244110540698000
      })

      m4 = ManifestScroll.insert(%Manifest{
        app_key: app_key_d,
        phase: :deployed,
        status: :disabled,
        dependencies: [],
        commands: [],
        events: [],
        created_at: 1697244110540698000,
        deployed_at: 1697244110540698000
      })

      [^m1, ^m2] = ManifestScroll.list_all_by_phase(:staged)
      [^m3, ^m4] = ManifestScroll.list_all_by_phase(:deployed)

      Enum.each([m1,m2,m3,m4], fn m -> ManifestScroll.remove(m) end)
    end
  end
end
