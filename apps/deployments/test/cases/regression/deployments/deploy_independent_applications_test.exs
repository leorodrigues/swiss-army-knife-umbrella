defmodule Deployments.DeployIndependentApplicationsTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :deployments

  alias Deployments.Interface.Topics
  alias Deployments.EmptyHandlers

  alias Deployments.Test.CommandTasker
  alias Deployments.TestReplyListener
  alias Deployments.TestNotificationsListener
  alias Deployments.TestTopics

  import Deployments.TestUtils

  @inbox "deployment-tests"

  @test_listeners [TestReplyListener, TestNotificationsListener]

  setup_all_environment do
    Hermes.set_dispatch_on_publish(:single_topic)
    Hermes.set_sleep_interval(5000)
    Process.sleep(Hermes.Dispatching.Coordinator.default_sleep_interval + 100)
    [ ]
  end

  setup ctx do
    case ctx do
      %{samples_file: f} -> load_samples(ctx, resolve_test_resource_path([f]))
      _ -> :ok
    end
  end

  setup do
    :meck.new([EmptyHandlers], [:passthrough])

    Task.Supervisor.start_link(name: CommandTasker)

    on_exit(fn -> :meck.unload end)
  end

  describe "Deploy independent applications" do
    @tag samples_file: "case1.terms"
    test "should conclude without errors", %{samples: samples} do

      with_listeners(@test_listeners, fn ->

        using_sample(samples, :put_req_a)
          |> submit_command(TestTopics.put_deployment_command_reply, Topics.put_deployment_command)

        using_sample(samples, :put_req_b)
          |> submit_command(TestTopics.put_deployment_command_reply, Topics.put_deployment_command)

        using_sample(samples, :put_req_c)
          |> submit_command(TestTopics.put_deployment_command_reply, Topics.put_deployment_command)


        Process.sleep(2000)


        using_sample(samples, :staged_a)
          |> wait_on(EmptyHandlers, :handle_put, TestTopics.put_deployment_command_reply, @inbox, 5000)
          |> ensure_call_count(1, EmptyHandlers, :handle_put, TestTopics.put_deployment_command_reply, @inbox)

        using_sample(samples, :staged_b)
          |> wait_on(EmptyHandlers, :handle_put, TestTopics.put_deployment_command_reply, @inbox, 5000)
          |> ensure_call_count(1, EmptyHandlers, :handle_put, TestTopics.put_deployment_command_reply, @inbox)

        using_sample(samples, :staged_c)
          |> wait_on(EmptyHandlers, :handle_put, TestTopics.put_deployment_command_reply, @inbox, 5000)
          |> ensure_call_count(1, EmptyHandlers, :handle_put, TestTopics.put_deployment_command_reply, @inbox)


        using_sample(samples, :deployed_a)
          |> wait_on(EmptyHandlers, :handle_put, TestTopics.put_deployment_command_reply, @inbox, 5000)
          |> ensure_call_count(1, EmptyHandlers, :handle_put, TestTopics.put_deployment_command_reply, @inbox)

        using_sample(samples, :deployed_b)
          |> wait_on(EmptyHandlers, :handle_put, TestTopics.put_deployment_command_reply, @inbox, 5000)
          |> ensure_call_count(1, EmptyHandlers, :handle_put, TestTopics.put_deployment_command_reply, @inbox)

        using_sample(samples, :deployed_c)
          |> wait_on(EmptyHandlers, :handle_put, TestTopics.put_deployment_command_reply, @inbox, 5000)
          |> ensure_call_count(1, EmptyHandlers, :handle_put, TestTopics.put_deployment_command_reply, @inbox)
      end)
    end
  end
end
