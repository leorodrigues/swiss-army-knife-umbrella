defmodule Deployments.Domain.AppKeyTest do
  use ExUnit.Case

  alias Deployments.Domain.AppKey

  describe "Deployments.Domain.AppKey.make_from_map" do
    test "should return an AppKey" do
      %AppKey{
        name: :test_app, version: "2.2.0"
      } = AppKey.make_from_map(%{
        app_name: :test_app, app_version: "2.2.0"
      })
    end

    test "should ask for the field :app_name" do
      error = {:error, {to_string(AppKey), :required_key, :app_name}}

      ^error = AppKey.make_from_map(%{app_version: "2.2.0"})
    end

    test "should ask for the field :app_version" do
      error = {:error, {to_string(AppKey), :required_key, :app_version}}

      ^error = AppKey.make_from_map(%{app_name: :test_app})
    end
  end

  describe "Deployments.Domain.AppKey.to_map" do
    test "should return a map containing everything" do
      result = %{app_name: :the_app, app_version: "2.0.0"}

      ^result = AppKey.to_map(%AppKey{name: :the_app, version: "2.0.0"})
    end
  end
end
