defmodule Deployments.Domain.DependencySetTest do
  use ExUnit.Case

  alias Deployments.Domain.DependencySet
  alias Deployments.Domain.Dependency
  alias Deployments.Domain.AppKey

  describe "Deployments.Domain.DependencySet.review_pending" do
    test "should do nothing if there are no dependencies nor app keys" do
      [ ] = DependencySet.review_pending([ ], [ ])
    end

    test "should do nothing if there are dependencies but no app keys" do
      deps = [%Dependency{status: :pending, app_key: %AppKey{name: :my_app, version: "1.0.0"}}]
      result = [%Dependency{status: :pending, app_key: %AppKey{name: :my_app, version: "1.0.0"}}]
      ^result = DependencySet.review_pending(deps, [ ])
    end

    test "should do nothing if there are app keys but no dependencies" do
      [ ] = DependencySet.review_pending([ ], [%AppKey{name: :my_app, version: "1.0.0"}])
    end

    test "should do nothing if there is a pending dependency but no matching app key" do
      deps = [%Dependency{status: :pending, app_key: %AppKey{name: :my_app, version: "1.0.0"}}]
      app_keys = [%AppKey{name: :app_1, version: "1.0.0"}, %AppKey{name: :my_app, version: "2.0.0"}]
      result = [%Dependency{status: :pending, app_key: %AppKey{name: :my_app, version: "1.0.0"}}]
      ^result = DependencySet.review_pending(deps, app_keys)
    end

    test "should do nothing if there are no pending dependencies" do
      deps = [%Dependency{status: :provided, app_key: %AppKey{name: :my_app, version: "2.0.0"}}]
      app_keys = [%AppKey{name: :my_app, version: "2.0.0"}]
      result = [%Dependency{status: :provided, app_key: %AppKey{name: :my_app, version: "2.0.0"}}]
      ^result = DependencySet.review_pending(deps, app_keys)
    end

    test "should change pending dependencies to provided" do
      deps = [%Dependency{status: :pending, app_key: %AppKey{name: :my_app, version: "2.0.0"}}]
      app_keys = [%AppKey{name: :my_app, version: "2.0.0"}]
      result = [%Dependency{status: :provided, app_key: %AppKey{name: :my_app, version: "2.0.0"}}]
      ^result = DependencySet.review_pending(deps, app_keys)
    end

    test "should keep dependency order" do
      app_keys = [
        %AppKey{name: :app_1, version: "1.0.0"},
        %AppKey{name: :my_app, version: "2.0.0"}
      ]
      deps = [
        %Dependency{status: :pending, app_key: %AppKey{name: :my_app, version: "2.0.0"}},
        %Dependency{status: :pending, app_key: %AppKey{name: :another_app, version: "1.0.0"}}
      ]
      result = [
        %Dependency{status: :provided, app_key: %AppKey{name: :my_app, version: "2.0.0"}},
        %Dependency{status: :pending, app_key: %AppKey{name: :another_app, version: "1.0.0"}}
      ]
      ^result = DependencySet.review_pending(deps, app_keys)
    end
  end
end
