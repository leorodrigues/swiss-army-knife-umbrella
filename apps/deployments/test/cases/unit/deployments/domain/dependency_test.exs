defmodule Deployments.Domain.DependencyTest do
  use ExUnit.Case

  alias Deployments.Domain.Dependency
  alias Deployments.Domain.AppKey

  describe "Deployments.Domain.Dependency.make_from_map" do
    test "should make a dependency from a map" do
      map = %{app_name: :the_app, app_version: "2.0.0", status: :provided}

      expected = %Dependency{
        app_key: %AppKey{name: :the_app, version: "2.0.0"},
        status: :provided
      }

      ^expected = Dependency.make_from_map(map)
    end

    test "should make a pending dependency from a map" do
      map = %{app_name: :the_app, app_version: "2.0.0"}

      expected = %Dependency{
        app_key: %AppKey{name: :the_app, version: "2.0.0"},
        status: :pending
      }

      ^expected = Dependency.make_from_map(map)
    end
  end

  describe "Deployments.Domain.Dependency.to_map" do
    test "should make a map from a dependency" do
      pending_dep = %Dependency{app_key: %AppKey{name: :some_name, version: "1.0.0"}}
      pending_map = %{app_name: :some_name, app_version: "1.0.0", status: :pending}
      ^pending_map = Dependency.to_map(pending_dep)

      provided_dep = %Dependency{app_key: %AppKey{name: :some_name, version: "1.0.0"}, status: :provided}
      provided_map = %{app_name: :some_name, app_version: "1.0.0", status: :provided}
      ^provided_map = Dependency.to_map(provided_dep)
    end
  end
end
