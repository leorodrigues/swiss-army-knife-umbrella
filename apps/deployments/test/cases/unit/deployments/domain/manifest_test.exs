defmodule Deployments.Domain.ManifestTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :deployments

  alias Deployments.Domain.Clock
  alias Deployments.Domain.AppKey
  alias Deployments.Domain.Manifest
  alias Deployments.Domain.Dependency
  alias Deployments.Domain.DependencySet

  import Deployments.TestUtils

  setup_all_environment do
    [ ]
  end

  setup ctx do
    case ctx do
      %{samples_file: f} -> load_samples(ctx, resolve_test_resource_path([f]))
      _ -> :ok
    end
  end

  setup do
    :meck.new([DependencySet])
    :meck.new([Clock, UUID], [:passthrough])
    on_exit(fn -> :meck.unload() end)
  end

  describe "Deployments.Domain.Manifest.review_pending_dependencies" do

    @tag samples_file: "case1.terms"
    test "should turn to :deployed if all dependencies where provided", %{samples: samples} do
      %{timestamp_set: timestamp_set, provided_deps: provided_deps} = samples

      timestamp_stack = stack(:timestamps, timestamp_set)

      :meck.expect(Clock, :timestamp, fn -> pop(timestamp_stack) end)
      :meck.expect(DependencySet, :review_pending, fn _, _ -> provided_deps end)

      %{staged: %Manifest{ } = staged, deployed: %Manifest{ } = deployed, app_keys: keys} = samples
      ^deployed = Manifest.review_pending_dependencies(staged, keys)

      :meck.wait(DependencySet, :review_pending, [staged.dependencies, keys], 1000)
    end

    @tag samples_file: "case2.terms"
    test "should turn to :deployed if there are no dependencies", %{samples: samples} do
      %{timestamp_set: timestamp_set} = samples

      timestamp_stack = stack(:timestamps, timestamp_set)

      :meck.expect(Clock, :timestamp, fn -> pop(timestamp_stack) end)
      :meck.expect(DependencySet, :review_pending, fn _, _ -> [ ] end)

      %{staged: staged, deployed: deployed, app_keys: app_keys} = samples

      ^deployed = Manifest.review_pending_dependencies(staged, app_keys)

      :meck.wait(DependencySet, :review_pending, [[ ], app_keys], 1000)
    end

    @tag samples_file: "case3.terms"
    test "should turn to :staged if at least one dependency is pending", %{samples: samples} do
      %{timestamp_set: timestamp_set} = samples
      timestamp_stack = stack(:timestamps, timestamp_set)
      :meck.expect(Clock, :timestamp, fn -> pop(timestamp_stack) end)

      %{staged: staged, deployed: deployed, app_keys: app_keys, reviewed_deps: deps} = samples

      :meck.expect(DependencySet, :review_pending, fn _, _ -> deps end)

      ^staged = Manifest.review_pending_dependencies(deployed, app_keys)

      :meck.wait(DependencySet, :review_pending, [deployed.dependencies, app_keys], 1000)
    end
  end

  describe "Deployments.Domain.Manifest.make_from_map" do

    @tag samples_file: "case4.terms"
    test "should make a complete manifest from a map", %{samples: samples} do
      %{manifest: manifest, map: map} = samples
      ^manifest = Manifest.make_from_map(map)
    end

    @tag samples_file: "case5.terms"
    test "should compute a creation date if not given", %{samples: samples} do
      %{manifest: manifest, map: map, timestamp: time} = samples
      :meck.expect(Clock, :timestamp, fn -> time end)
      ^manifest = Manifest.make_from_map(map)
    end

    @tag samples_file: "case6.terms"
    test "should map the smallest possible map", %{samples: samples} do
      %{manifest: manifest, map: map, timestamp: time} = samples
      :meck.expect(Clock, :timestamp, fn -> time end)
      ^manifest = Manifest.make_from_map(map)
    end

    test "should complain about a pending app key at the root" do
      error = {:error, {to_string(Manifest), :required_key, :app_key}}

      ^error = Manifest.make_from_map(%{ })
    end

    test "should complain about a pending app name" do
      error = {:error, {to_string(Manifest), {
        to_string(AppKey), :required_key, :app_name
      }}}

      ^error = Manifest.make_from_map(%{
        app_key: %{ }
      })
    end

    test "should complain about a pending app version" do
      error = {:error, {to_string(Manifest), {
        to_string(AppKey), :required_key, :app_version
      }}}

      ^error = Manifest.make_from_map(%{
        app_key: %{app_name: :app}
      })
    end

    test "should complain about a pending app name on a dependency" do
      error = {:error, {to_string(Manifest), {to_string(Dependency), {
        to_string(AppKey), :required_key, :app_name
      }}}}

      ^error = Manifest.make_from_map(%{
        app_key: %{app_name: :some_app, app_version: "1.0.0"},
        dependencies: [%{ }]
      })
    end
  end

  describe "Deployments.Domain.Manifest.to_map" do
    test "should make a map out of a manifest" do
      manifest = %Manifest{
        id: "112358",
        app_key: %AppKey{name: :app, version: "1.0"},
        phase: :deployed,
        status: :disabled,
        dependencies: [
          %Dependency{app_key: %AppKey{name: :req_app_a, version: "2.0"}, status: :pending},
          %Dependency{app_key: %AppKey{name: :req_app_b, version: "1.0"}, status: :provided},
        ],
        events: ["some-event"],
        commands: ["some-command"],
        created_at: 1697244110540698000,
        deployed_at: 1697244110540698000
      }

      manifest_map = %{
        id: "112358",
        app_key: %{app_name: :app, app_version: "1.0"},
        phase: :deployed,
        status: :disabled,
        dependencies: [
          %{app_name: :req_app_a, app_version: "2.0", status: :pending},
          %{app_name: :req_app_b, app_version: "1.0", status: :provided}
        ],
        events: ["some-event"],
        commands: ["some-command"],
        created_at: 1697244110540698000,
        deployed_at: 1697244110540698000
      }

      ^manifest_map = Manifest.to_map(manifest)
    end
  end

end
