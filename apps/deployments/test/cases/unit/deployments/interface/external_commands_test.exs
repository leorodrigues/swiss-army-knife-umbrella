defmodule Deployments.Interface.Outbound.ExternalCommandsTest do
  use ExUnit.Case

  alias Deployments.Domain.AppKey
  alias Deployments.Domain.Manifest
  alias Deployments.Interface.Outbound.ExternalCommands
  alias Deployments.Interface.Topics

  alias Hermes.Piping

  setup do
    :meck.new(Piping)
    on_exit(fn -> :meck.unload end)
  end

  describe "Deployments.Interface.Outbound.ExternalCommands.notify_deployment_phase_changed" do
    test "should notify a staged deployment" do
      timestamp = System.system_time(:microsecond)

      :meck.expect(Piping, :submit_void_command, fn _, _ -> :ok end)

      manifest = %Manifest{
        app_key: %AppKey{name: :some_app, version: "1.0.0"},
        created_at: timestamp
      }

      map = %{
        level: :info,
        meta: %{
          app_name: :some_app,
          app_version: "1.0.0"
        },
        timestamp: timestamp,
        token: "deployment-staged",
        message: "Deployment staged"
      }

      :ok = ExternalCommands.notify_deployment_phase_changed(manifest)

      :meck.wait(Piping, :submit_void_command, [Topics.put_notification_command, map], 3000)
    end

    test "should notify a confirmed deployment" do
      timestamp = System.system_time(:microsecond)

      :meck.expect(Piping, :submit_void_command, fn _, _ -> :ok end)

      manifest = %Manifest{
        app_key: %AppKey{name: :some_app, version: "1.0.0"},
        phase: :deployed,
        deployed_at: timestamp
      }

      map = %{
        level: :info,
        meta: %{
          app_name: :some_app,
          app_version: "1.0.0"
        },
        timestamp: timestamp,
        token: "deployment-confirmed",
        message: "Deployment confirmed"
      }

      :ok = ExternalCommands.notify_deployment_phase_changed(manifest)

      :meck.wait(Piping, :submit_void_command, [Topics.put_notification_command, map], 3000)
    end
  end
end
