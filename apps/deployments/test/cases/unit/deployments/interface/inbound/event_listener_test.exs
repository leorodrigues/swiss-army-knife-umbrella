defmodule Deployments.Interface.Inbound.EventListenerTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :deployments

  alias Deployments.Interface.Topics
  alias Deployments.Interface.PayloadMapping
  alias Deployments.Interface.Inbound.EventListener
  alias Deployments.Interface.Outbound.ExternalCommands

  alias Hermes.Piping

  import Deployments.TestUtils

  @modules_to_stub [PayloadMapping, ExternalCommands, Piping]

  setup ctx do
    case ctx do
      %{samples_file: f} -> load_samples(ctx, resolve_test_resource_path([f]))
      _ -> :ok
    end
  end

  setup do
    :meck.new(@modules_to_stub)
    on_exit(fn -> :meck.unload end)
  end

  describe "Deployments.Interface.Inbound.EventListener, listen to deployment staged" do

    @tag samples_file: "case1.terms"
    test "should notify phase change and re-route the event", %{samples: s} do
      manifest = using_sample(s, :staged_manifest)

      :meck.expect(PayloadMapping, :to_manifest, fn _ -> manifest end)
      :meck.expect(PayloadMapping, :to_command_response, fn _ -> :fake_payload end)
      :meck.expect(ExternalCommands, :notify_deployment_phase_changed, fn _ -> :ok end)
      :meck.expect(Piping, :submit_void_command, fn _, _ -> :ok end)

      EventListener.run_org_leonardo_sak_deployments_events_deployment_staged(:fake_payload, [ ])

      :meck.wait(ExternalCommands, :notify_deployment_phase_changed, [manifest], 5000)
      :meck.wait(Piping, :submit_void_command, [Topics.react_to_mutation, :fake_payload], 5000)
    end

  end

  describe "Deployments.Interface.Inbound.EventListener, listen to deployment confirmed" do
    @tag samples_file: "case1.terms"
    test "should notify phase change and re-route the event", %{samples: s} do
      manifest = using_sample(s, :deployed_manifest)

      :meck.expect(PayloadMapping, :to_manifest, fn _ -> manifest end)
      :meck.expect(PayloadMapping, :to_command_response, fn _ -> :fake_payload end)
      :meck.expect(ExternalCommands, :notify_deployment_phase_changed, fn _ -> :ok end)
      :meck.expect(Piping, :submit_void_command, fn _, _ -> :ok end)

      EventListener.run_org_leonardo_sak_deployments_events_deployment_confirmed(:fake_payload, [ ])

      :meck.wait(ExternalCommands, :notify_deployment_phase_changed, [manifest], 5000)
      :meck.wait(Piping, :submit_void_command, [Topics.react_to_mutation, :fake_payload], 5000)
    end
  end
end
