defmodule Deployments.Interface.Inbound.GetDeploymentListenerTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :deployments

  alias Deployments.Interface.PayloadMapping

  alias Deployments.Interface.Inbound.GetDeploymentListener

  alias Deployments.UseCases.GetDeployment

  import Deployments.TestUtils

  @modules_to_stub [
    PayloadMapping,
    GetDeployment
  ]

  setup ctx do
    case ctx do
      %{samples_file: f} -> load_samples(ctx, resolve_test_resource_path([f]))
      _ -> :ok
    end
  end

  setup do
    :meck.new(@modules_to_stub)
    on_exit(fn -> :meck.unload end)
  end

  describe "Deployments.Interface.Inbound.GetDeploymentListener" do

    @tag samples_file: "case1.terms"
    test "should go about its business", %{samples: s} do
      app_key = using_sample(s, :app_key)
      manifest = using_sample(s, :manifest)

      :meck.expect(PayloadMapping, :to_app_key, fn _ -> app_key end)
      :meck.expect(GetDeployment, :invoke, fn ^app_key -> manifest end)
      :meck.expect(PayloadMapping, :to_command_response, fn ^manifest -> :fake_output end)

      :fake_output = GetDeploymentListener.run_org_leonardo_sak_deployments_commands_get(:fake_input , [ ])
    end

    test "should respond with error in case of malformed payload" do
      :meck.expect(PayloadMapping, :to_app_key, fn _ -> {:error, :fake_reason} end)

      {:error, :fake_reason} = GetDeploymentListener.run_org_leonardo_sak_deployments_commands_get(:fake_input, [ ])
    end

    @tag samples_file: "case1.terms"
    test "should respond with error in case of an actual business error", %{samples: s} do
      app_key = using_sample(s, :app_key)

      :meck.expect(PayloadMapping, :to_app_key, fn _ -> app_key end)
      :meck.expect(GetDeployment, :invoke, fn ^app_key -> {:error, :fake_reason} end)

      {:error, :fake_reason} = GetDeploymentListener.run_org_leonardo_sak_deployments_commands_get(:fake_input, [ ])
    end
  end
end
