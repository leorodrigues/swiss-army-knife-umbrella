defmodule Deployments.Interface.Inbound.MutationCommandListenerTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :deployments

  alias Deployments.Interface.PayloadMapping
  alias Deployments.Interface.Inbound.MutationCommandListener

  alias Deployments.Interface.Outbound.DelayedManifestReplyPipeline

  alias Deployments.UseCases.Dependencies.TryFulfillingDeployment
  alias Deployments.UseCases.Dependencies.TryApplyingDeployment

  @modules_to_stub [
    PayloadMapping,
    DelayedManifestReplyPipeline,
    TryFulfillingDeployment
  ]

  @modules_to_spy_on [Logger]

  import Deployments.TestUtils

  setup ctx do
    case ctx do
      %{samples_file: f} -> load_samples(ctx, resolve_test_resource_path([f]))
      _ -> :ok
    end
  end

  setup do
    :meck.new(@modules_to_stub)
    :meck.new(@modules_to_spy_on, [:passthrough])
    on_exit(fn -> :meck.unload end)
  end

  describe "Deployments.Interface.Inbound.MutationCommandListener, when responding to staged payload" do
    @tag samples_file: "case1.terms"
    test "should invoke the use case successfully", %{samples: s} do
      input = using_sample(s, :staged_payload)
      manifest = using_sample(s, :staged_manifest)

      :meck.expect(PayloadMapping, :to_manifest, fn _ -> manifest end)
      :meck.expect(DelayedManifestReplyPipeline, :convey, fn _, _ -> :ok end)
      :meck.expect(TryFulfillingDeployment, :invoke, fn ^manifest -> :result end)

      :result = MutationCommandListener.run_org_leonardo_sak_deployments_commands_react_to_mutation(input, [ ])
    end
  end

  describe "Deployments.Interface.Inbound.MutationCommandListener, when responding to deployed payload" do
    @tag samples_file: "case2.terms"
    test "should invoke the use case successfully", %{samples: s} do
      input = using_sample(s, :deployed_payload)
      manifest = using_sample(s, :deployed_manifest)

      :meck.expect(PayloadMapping, :to_manifest, fn _ -> manifest end)
      :meck.expect(DelayedManifestReplyPipeline, :convey, fn _, _ -> :ok end)
      :meck.expect(TryApplyingDeployment, :invoke, fn ^manifest -> :result end)

      :result = MutationCommandListener.run_org_leonardo_sak_deployments_commands_react_to_mutation(input, [ ])
    end
  end

  describe "Deployments.Interface.Inbound.MutationCommandListener, when responding to unknown payload" do
    test "should warn about the unknown payload" do
      log_line = [comment: "Unknown mutation", payload: "clearly the wrong payload"]
      :ok = MutationCommandListener.run_org_leonardo_sak_deployments_commands_react_to_mutation("clearly the wrong payload", [ ])
      :meck.wait(Logger, :__do_log__, [:warning, log_line, :_, :_], 3000)
    end
  end
end
