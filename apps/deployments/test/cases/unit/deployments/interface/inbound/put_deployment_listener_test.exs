defmodule Deployments.Interface.Inbound.PutDeploymentListenerTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :deployments

  alias Deployments.Interface.PayloadMapping

  alias Deployments.Interface.Inbound.PutDeploymentListener

  alias Deployments.UseCases.PutDeployment

  import Deployments.TestUtils

  @modules_to_stub [PayloadMapping, PutDeployment]

  setup ctx do
    case ctx do
      %{samples_file: f} -> load_samples(ctx, resolve_test_resource_path([f]))
      _ -> :ok
    end
  end

  setup do
    :meck.new(@modules_to_stub)
    on_exit(fn -> :meck.unload end)
  end

  describe "Deployments.Interface.Inbound.PutDeploymentListener" do
    @tag samples_file: "case1.terms"
    test "should go about its business", %{samples: s} do
      input_manifest = using_sample(s, :input_manifest)
      staged_manifest = using_sample(s, :staged_manifest)
      staged_id = staged_manifest.id

      :meck.expect(PayloadMapping, :to_manifest, fn :fake_payload -> input_manifest end)
      :meck.expect(PutDeployment, :invoke, fn ^input_manifest -> staged_manifest end)
      :meck.expect(PayloadMapping, :to_command_response, fn ^staged_manifest -> %{id: staged_id} end)

      ^staged_id = PutDeploymentListener.run_org_leonardo_sak_deployments_commands_put(:fake_payload , [ ])
    end
  end
end
