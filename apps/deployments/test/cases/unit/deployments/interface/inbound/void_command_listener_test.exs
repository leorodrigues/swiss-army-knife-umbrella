defmodule Deployments.Interface.Inbound.VoidCommandListenerTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :deployments

  alias Deployments.Interface.Inbound.VoidCommandListener

  alias Deployments.Interface.PayloadMapping

  alias Deployments.UseCases.ReviewDeployment

  import Deployments.TestUtils

  @modules_to_stub [PayloadMapping, ReviewDeployment]

  setup ctx do
    case ctx do
      %{samples_file: f} -> load_samples(ctx, resolve_test_resource_path([f]))
      _ -> :ok
    end
  end

  setup do
    :meck.new(@modules_to_stub)
    on_exit(fn -> :meck.unload end)
  end

  describe "Deployments.Interface.Inbound.VoidCommandListener, when handling a request" do
    @tag samples_file: "case1.terms"
    test "should invoke the review deployment use case", %{samples: s} do
      app_key = using_sample(s, :app_key)

      :meck.expect(PayloadMapping, :to_app_key, fn :input_payload -> app_key end)
      :meck.expect(ReviewDeployment, :invoke, fn ^app_key -> :fake_output end)

      :fake_output = VoidCommandListener.run_org_leonardo_sak_deployments_commands_review(:input_payload, [ ])
    end
  end
end
