defmodule Deployments.Interface.Outbound.DefaultEventEmitterTest do
  use ExUnit.Case

  alias Deployments.Domain.Manifest
  alias Deployments.Interface.Topics
  alias Deployments.Interface.PayloadMapping
  alias Deployments.Interface.Outbound.DefaultEventEmitter

  alias Hermes.Piping

  setup do
    :meck.new([Piping, PayloadMapping])
    on_exit(fn -> :meck.unload() end)
  end

  describe "Deployments.Interfaces.Events.fire_deployment_staged" do
    test "should send a response payload" do
      :meck.expect(PayloadMapping, :to_command_response, fn _ -> :response end)
      :meck.expect(Piping, :trigger_event, fn _, _ -> :ok end)

      :ok = DefaultEventEmitter.fire_deployment_staged(%Manifest{id: "7"})

      :meck.wait(PayloadMapping, :to_command_response, [%Manifest{id: "7"}], 1000)
      :meck.wait(Piping, :trigger_event, [Topics.deployment_staged_event, :response], 1000)
    end
  end

  describe "Deployments.Interfaces.Events.fire_deployment_confirmed" do
    test "should send a response payload" do
      :meck.expect(PayloadMapping, :to_command_response, fn _ -> :response end)
      :meck.expect(Piping, :trigger_event, fn _, _ -> :ok end)

      :ok = DefaultEventEmitter.fire_deployment_confirmed(%Manifest{id: "7"})

      :meck.wait(PayloadMapping, :to_command_response, [%Manifest{id: "7"}], 1000)
      :meck.wait(Piping, :trigger_event, [Topics.deployment_confirmed_event, :response], 1000)
    end
  end
end
