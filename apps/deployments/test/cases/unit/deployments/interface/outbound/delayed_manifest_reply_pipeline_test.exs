defmodule Deployments.Interface.Outbound.DelayedManifestReplyPipelineTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :deployments

  alias Deployments.Interface.Outbound.DelayedManifestReplyPipeline

  alias Deployments.Interface.PayloadMapping

  alias Deployments.Interface.CommandContextServer

  alias SakMessaging.CommandContext

  alias Hermes.Piping.Outbound.MakeCommandReplyPipe
  alias Hermes.Piping.Outbound.PublishReplyPipe

  import Deployments.TestUtils

  @modules_to_stub [
    CommandContext,
    PayloadMapping,
    MakeCommandReplyPipe,
    PublishReplyPipe
  ]

  setup ctx do
    case ctx do
      %{samples_file: f} -> load_samples(ctx, resolve_test_resource_path([f]))
      _ -> :ok
    end
  end

  setup do
    :meck.new(@modules_to_stub)
    on_exit(fn -> :meck.unload end)
  end

  describe "Deployments.Interface.Outbound.DelayedManifestReplyPipeline" do

    @tag samples_file: "case1.terms"
    test "should load the command context, publish the manifest, and keep the context", %{samples: s} do
      manifest = using_sample(s, :staged_manifest)
      manifest_id = manifest.id

      :meck.expect(CommandContext, :load, fn CommandContextServer, ^manifest_id -> :ok end)
      :meck.expect(PayloadMapping, :to_command_response, fn ^manifest -> :parsed_payload end)
      :meck.expect(MakeCommandReplyPipe, :convey, fn :parsed_payload, _ -> :fake_reply end)
      :meck.expect(PublishReplyPipe, :convey, fn :fake_reply, _ -> :fake_result end)

      :fake_result = DelayedManifestReplyPipeline.convey(manifest)

      0 = :meck.num_calls(CommandContext, :drop, :_)
    end

    @tag samples_file: "case1.terms"
    test "should load the command context, publish the manifest, and drop the context", %{samples: s} do
      manifest = using_sample(s, :staged_manifest)
      manifest_id = manifest.id

      :meck.expect(CommandContext, :load, fn CommandContextServer, ^manifest_id -> :ok end)
      :meck.expect(CommandContext, :drop, fn CommandContextServer, ^manifest_id -> :ok end)
      :meck.expect(PayloadMapping, :to_command_response, fn ^manifest -> :parsed_payload end)
      :meck.expect(MakeCommandReplyPipe, :convey, fn :parsed_payload, _ -> :fake_reply end)
      :meck.expect(PublishReplyPipe, :convey, fn :fake_reply, _ -> :fake_result end)

      :fake_result = DelayedManifestReplyPipeline.convey(manifest, [drop_context: true])

      :meck.wait(CommandContext, :drop, [CommandContextServer, manifest_id], 1)
    end
  end
end
