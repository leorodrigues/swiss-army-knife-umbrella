defmodule Deployments.Interface.PayloadMappingTest do
  use ExUnit.Case

  alias Deployments.Domain.Manifest
  alias Deployments.Domain.AppKey

  import Deployments.Interface.PayloadMapping

  setup do
    :meck.new([Manifest, AppKey])
    on_exit(fn -> :meck.unload() end)
  end

  describe "Deployments.Interface.PayloadMapping.to_app_key" do
    test "should make an app_key from a map" do
      :meck.expect(AppKey, :make_from_map, fn _ -> %AppKey{
        name: :test_app, version: "2.2.0"
      } end)

      %AppKey{
        name: :test_app, version: "2.2.0"
      } = to_app_key(%{app_name: :test_app, app_version: "2.2.0"})
    end
  end

  describe "Deployments.Interface.PayloadMapping.to_manifest" do
    test "should make a manifest from a map" do
      :meck.expect(Manifest, :make_from_map, fn _ -> %Manifest{
        app_key: {:some_app, "1.0.0"}
      } end)

      expected_result = %Manifest{
        app_key: {:some_app, "1.0.0"}
      }

      ^expected_result = to_manifest(%{
        app_key: %{app_name: :some_app, app_version: "1.0.0"}
      })

      :meck.wait(Manifest, :make_from_map, [%{
        app_key: %{app_name: :some_app, app_version: "1.0.0"}
      }], 1000)
    end
  end

  describe "Deployments.Interface.PayloadMapping.to_command_response" do
    test "should turn a manifest to a map" do
      :meck.expect(Manifest, :to_map, fn _ -> %{id: "112358"} end)
      %{id: "112358"} = to_command_response(%Manifest{id: "112358"})
      :meck.wait(Manifest, :to_map, [%Manifest{id: "112358"}], 1000)
    end

    test "should echo the :not_found return" do
      :not_found = to_command_response(:not_found)
    end
  end
end
