defmodule Deployments.State.DependencySetMappingTest do
  use ExUnit.Case

  alias Deployments.Domain.Dependency
  alias Deployments.Domain.AppKey

  import Deployments.State.DependencySetMapping

  describe "Deployments.State.DependencySetMapping.dependency_set_to_record_set" do
    test "should turn a set of dependencies to a set of tuples" do
      given_dependencies = [
        %Dependency{app_key: %AppKey{name: :app_1, version: "1.2"}, status: :pending},
        %Dependency{app_key: %AppKey{name: :app_2, version: "2.3"}, status: :provided}
      ]

      expected_tuples = [
        {%AppKey{name: :app_1, version: "1.2"}, :pending},
        {%AppKey{name: :app_2, version: "2.3"}, :provided}
      ]

      ^expected_tuples = dependency_set_to_record_set(given_dependencies)
    end
  end

  describe "Deployments.State.DependencySetMapping.record_set_to_dependency_set" do
    test "should turn a set of tuples to a set of dependencies" do
      expected_dependencies = [
        %Dependency{app_key: %AppKey{name: :app_1, version: "1.2"}, status: :pending},
        %Dependency{app_key: %AppKey{name: :app_2, version: "2.3"}, status: :provided}
      ]

      given_tuples = [
        {%AppKey{name: :app_1, version: "1.2"}, :pending},
        {%AppKey{name: :app_2, version: "2.3"}, :provided}
      ]

      ^expected_dependencies = record_set_to_dependency_set(given_tuples)
    end
  end
end
