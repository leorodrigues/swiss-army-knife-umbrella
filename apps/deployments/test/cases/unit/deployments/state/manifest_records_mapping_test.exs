defmodule Deployments.State.ManifestScrollMappingTest do
  use ExUnit.Case

  alias Deployments.Domain.AppKey
  alias Deployments.Domain.Manifest
  alias Deployments.Domain.Dependency

   import Deployments.State.ManifestRecordMapping

  describe "Deployments.State.ManifestRecordMapping.manifest_to_record" do
    test "should turn a manifest into a tuple" do
      manifest = %Manifest{
        id: "112358",
        app_key: %AppKey{name: :some_app, version: "1.0.0"},
        dependencies: [
          %Dependency{app_key: %AppKey{name: :req_app_1, version: "2.0.0"}, status: :pending},
          %Dependency{app_key: %AppKey{name: :req_app_2, version: "2.1.0"}, status: :provided}
        ],
        events: ["some-event"],
        commands: ["some-command"],
        created_at: 1697244110540698000,
        deployed_at: 1697244110540698000,
        phase: :staged,
        status: :disabled
      }

      tuple = {
        "112358",
        %AppKey{name: :some_app, version: "1.0.0"},
        [
          {%AppKey{name: :req_app_1, version: "2.0.0"}, :pending},
          {%AppKey{name: :req_app_2, version: "2.1.0"}, :provided},
        ],
        ["some-event"],
        ["some-command"],
        :staged,
        :disabled,
        1697244110540698000,
        1697244110540698000
      }

      ^tuple = manifest_to_record(manifest)
    end
  end

  describe "Deployments.State.ManifestRecordMapping.record_to_manifest" do
    test "should turn a tuple back into a manifest" do
      manifest = %Manifest{
        id: "112358",
        app_key: %AppKey{name: :some_app, version: "1.0.0"},
        dependencies: [
          %Dependency{app_key: %AppKey{name: :req_app_1, version: "2.0.0"}, status: :pending},
          %Dependency{app_key: %AppKey{name: :req_app_2, version: "2.1.0"}, status: :provided}
        ],
        events: ["some-event"],
        commands: ["some-command"],
        created_at: 1697244110540698000,
        deployed_at: 1697244110540698000,
        phase: :staged,
        status: :disabled
      }

      tuple = {
        "112358",
        %AppKey{name: :some_app, version: "1.0.0"},
        [
          {%AppKey{name: :req_app_1, version: "2.0.0"}, :pending},
          {%AppKey{name: :req_app_2, version: "2.1.0"}, :provided},
        ],
        ["some-event"],
        ["some-command"],
        :staged,
        :disabled,
        1697244110540698000,
        1697244110540698000
      }

      ^manifest = record_to_manifest(tuple)
    end
  end
end
