defmodule Deployments.UseCases.Dependencies.TryApplyingDeploymentTest do
  use ExUnit.Case

  alias Deployments.Domain.AppKey
  alias Deployments.Domain.Manifest

  alias Deployments.UseCases.Dependencies.TryApplyingDeployment

  alias Deployments.State.ManifestScroll

  alias Deployments.Interface.Outbound.DefaultEventEmitter

  alias Deployments.DummyModule

  @modules_to_stub [ManifestScroll, DefaultEventEmitter, Manifest]

  setup do
    :meck.new(@modules_to_stub)
    on_exit(fn -> :meck.unload end)
  end

  describe "Deployments.UseCases.Dependencies.TryApplyingDeployment.make_child_spec" do
    test "should produce a child spec" do

      find_manifest_by_phase = &DummyModule.find_manifest_by_phase/1
      fire_deployment_confirmed = &DummyModule.fire_deployment_confirmed/1
      fire_deployment_application_failed = &DummyModule.fire_deployment_application_failed/1
      update_manifest = &DummyModule.update_manifest/1

      {TryApplyingDeployment, init_args: [
        find_manifest_by_phase: ^find_manifest_by_phase,
        fire_deployment_confirmed: ^fire_deployment_confirmed,
        fire_deployment_application_failed: ^fire_deployment_application_failed,
        update_manifest: ^update_manifest
      ]} = TryApplyingDeployment.make_child_spec(
        find_manifest_by_phase,
        fire_deployment_confirmed,
        fire_deployment_application_failed,
        update_manifest
      )
    end
  end

  describe "Deployments.UseCases.Dependencies.TryApplyingDeployment.invoke" do
    test "should apply a manifest to other manifests, update all and fire events" do
      app_key_a = %AppKey{name: :app_a, version: "1.2"}
      app_key_b = %AppKey{name: :app_b, version: "1.2"}

      deployed_manifest = %Manifest{app_key: app_key_a, phase: :deployed}
      unfulfilled_manifest = %Manifest{app_key: app_key_b}
      fulfilled_manifest = %Manifest{app_key: app_key_b, phase: :deployed}

      :meck.expect(ManifestScroll, :list_all_by_phase, fn _ -> [unfulfilled_manifest] end)
      :meck.expect(Manifest, :review_pending_dependencies, fn _, _ -> fulfilled_manifest end)
      :meck.expect(ManifestScroll, :update, fn _ -> :ok end)
      :meck.expect(DefaultEventEmitter, :fire_deployment_confirmed, fn _ -> :ok end)

      :ok = TryApplyingDeployment.invoke(deployed_manifest)

      :meck.wait(ManifestScroll, :list_all_by_phase, [:staged], 1000)
      :meck.wait(Manifest, :review_pending_dependencies, [unfulfilled_manifest, [app_key_a]], 1000)
      :meck.wait(ManifestScroll, :update, [fulfilled_manifest], 1000)
      :meck.wait(DefaultEventEmitter, :fire_deployment_confirmed, [fulfilled_manifest], 3000)
    end

    test "should apply a manifest to other manifests, update but fire no events" do
      app_key_a = %AppKey{name: :app_a, version: "1.2"}
      app_key_b = %AppKey{name: :app_b, version: "1.2"}

      deployed_manifest = %Manifest{app_key: app_key_a, phase: :deployed}
      unfulfilled_manifest = %Manifest{app_key: app_key_b}

      :meck.expect(ManifestScroll, :list_all_by_phase, fn _ -> [unfulfilled_manifest] end)
      :meck.expect(Manifest, :review_pending_dependencies, fn _, _ -> unfulfilled_manifest end)
      :meck.expect(ManifestScroll, :update, fn _ -> :ok end)
      :meck.expect(DefaultEventEmitter, :fire_deployment_confirmed, fn _ -> :ok end)

      :ok = TryApplyingDeployment.invoke(deployed_manifest)

      :meck.wait(ManifestScroll, :list_all_by_phase, [:staged], 1000)
      :meck.wait(Manifest, :review_pending_dependencies, [unfulfilled_manifest, [app_key_a]], 1000)
      :meck.wait(ManifestScroll, :update, [unfulfilled_manifest], 1000)
      :meck.wait(0, DefaultEventEmitter, :fire_deployment_confirmed, :_, 3000)
    end

    test "should fire a deployment_application_failed_event if the update fails" do
      app_key_a = %AppKey{name: :app_a, version: "1.2"}
      app_key_b = %AppKey{name: :app_b, version: "1.2"}

      deployed_manifest = %Manifest{app_key: app_key_a, phase: :deployed}
      unfulfilled_manifest = %Manifest{app_key: app_key_b}
      fulfilled_manifest = %Manifest{app_key: app_key_b, phase: :deployed}

      :meck.expect(ManifestScroll, :list_all_by_phase, fn _ -> [unfulfilled_manifest] end)
      :meck.expect(Manifest, :review_pending_dependencies, fn _, _ -> fulfilled_manifest end)
      :meck.expect(ManifestScroll, :update, fn _ -> {:error, "fake error"} end)
      :meck.expect(DefaultEventEmitter, :fire_deployment_application_failed, fn _ -> :ok end)

      :ok = TryApplyingDeployment.invoke(deployed_manifest)

      :meck.wait(ManifestScroll, :list_all_by_phase, [:staged], 1000)
      :meck.wait(Manifest, :review_pending_dependencies, [unfulfilled_manifest, [app_key_a]], 1000)
      :meck.wait(ManifestScroll, :update, [fulfilled_manifest], 1000)
      :meck.wait(DefaultEventEmitter, :fire_deployment_application_failed, [{:error, :_}], 3000)
    end
  end
end
