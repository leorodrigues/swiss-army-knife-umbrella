defmodule Deployments.UseCases.Dependencies.TryFulfillingDeploymentTest do
  use ExUnit.Case

  alias Deployments.Domain.AppKey
  alias Deployments.Domain.Manifest

  alias Deployments.State.ManifestScroll

  alias Deployments.UseCases.Dependencies.TryFulfillingDeployment

  alias Deployments.Interface.Outbound.DefaultEventEmitter

  @modules [ManifestScroll, DefaultEventEmitter, Manifest]

  setup do
    :meck.new(@modules)
    on_exit(fn -> :meck.unload end)
  end

  describe "Deployments.UseCases.Dependencies.TryFulfillingDeployment.invoke" do
    test "should resolve deps, update and fire an event at the end" do
      app_key = %AppKey{name: :app, version: "1.0"}

      staged_manifest = %Manifest{app_key: app_key}

      deployed_manifest = %Manifest{staged_manifest|phase: :deployed}

      app_keys = [%AppKey{name: :req_app, version: "1.0"}]

      invocation = fn _, _ -> app_keys end

      :meck.expect(ManifestScroll, :list_all_app_keys_by_phase_and_status, invocation)
      :meck.expect(ManifestScroll, :find_by_app_key, fn _ -> staged_manifest end)
      :meck.expect(Manifest, :review_pending_dependencies, fn _, _ -> deployed_manifest end)
      :meck.expect(ManifestScroll, :update, fn _ -> :ok end)
      :meck.expect(DefaultEventEmitter, :fire_deployment_confirmed, fn _ -> :ok end)

      deployed_manifest = TryFulfillingDeployment.invoke(staged_manifest)

      :meck.wait(ManifestScroll, :list_all_app_keys_by_phase_and_status,
        [:deployed, :enabled], 1000)
      :meck.wait(Manifest, :review_pending_dependencies,
        [staged_manifest, app_keys], 1000)
      :meck.wait(ManifestScroll, :update,
        [deployed_manifest], 1000)
      :meck.wait(DefaultEventEmitter, :fire_deployment_confirmed,
        [deployed_manifest], 3000)
    end

    test "should resolve deps, update but no event is fired afterwards" do
      app_key = %AppKey{name: :app, version: "1.0"}

      staged_manifest = %Manifest{app_key: app_key}

      app_keys = [%AppKey{name: :req_app, version: "1.0"}]

      invocation = fn _, _ -> app_keys end

      :meck.expect(ManifestScroll, :list_all_app_keys_by_phase_and_status, invocation)
      :meck.expect(ManifestScroll, :find_by_app_key, fn _ -> staged_manifest end)
      :meck.expect(Manifest, :review_pending_dependencies, fn _, _ -> staged_manifest end)
      :meck.expect(ManifestScroll, :update, fn _ -> :ok end)

      staged_manifest = TryFulfillingDeployment.invoke(staged_manifest)

      :meck.wait(ManifestScroll, :list_all_app_keys_by_phase_and_status,
        [:deployed, :enabled], 1000)
      :meck.wait(Manifest, :review_pending_dependencies,
        [staged_manifest, app_keys], 1000)
      :meck.wait(ManifestScroll, :update,
        [staged_manifest], 1000)
      :meck.wait(0, DefaultEventEmitter, :fire_deployment_confirmed, :_, 3000)
    end
  end

end
