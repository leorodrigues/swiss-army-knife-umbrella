defmodule Deployments.UseCases.GetDeploymentTest do
  use ExUnit.Case

  alias Deployments.Domain.AppKey
  alias Deployments.Domain.Manifest

  alias Deployments.UseCases.GetDeployment

  alias Deployments.State.ManifestScroll

  setup do
    :meck.new([ManifestScroll])
    on_exit(fn -> :meck.unload() end)
  end

  describe "Deployments.UseCases.GetDeployment.invoke" do
    test "when manifest is not found, should just return :not_found" do
      invocation = fn _ -> :not_found end
      :meck.expect(ManifestScroll, :find_by_app_key, invocation)

      :not_found = GetDeployment.invoke(%AppKey{name: :fake_app, version: "1.0.0"})

      :meck.wait(ManifestScroll, :find_by_app_key, [
        %AppKey{name: :fake_app, version: "1.0.0"}
      ], 1000)
    end

    test "when manifest is found, it should be returned" do
      manifest = %Manifest{app_key: %AppKey{name: :fake_app, version: "1.0.0"}}
      invocation = fn _ -> manifest end
      :meck.expect(ManifestScroll, :find_by_app_key, invocation)

      ^manifest = GetDeployment.invoke(%AppKey{name: :fake_app, version: "1.0.0"})

      :meck.wait(ManifestScroll, :find_by_app_key, [
        %AppKey{name: :fake_app, version: "1.0.0"}
      ], 1000)
    end
  end

end
