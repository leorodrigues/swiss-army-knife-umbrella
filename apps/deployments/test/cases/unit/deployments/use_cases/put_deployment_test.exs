defmodule Deployments.UseCases.PutDeploymentTest do
  use ExUnit.Case

  alias Deployments.Domain.AppKey
  alias Deployments.Domain.Manifest

  alias Deployments.UseCases.PutDeployment

  alias Deployments.State.ManifestScroll

  alias Deployments.Interface.Outbound.DefaultEventEmitter

  @modules [ManifestScroll, DefaultEventEmitter]

  setup do
    :meck.new(@modules)
    on_exit(fn -> :meck.unload end)
  end

  describe "Deployments.UseCases.PutDeployment.invoke" do
    test "should create a new manifest and fire the 'deployment_staged' event" do
      fake_manifest = %Manifest{app_key: %AppKey{name: :fake_app, version: "1.0.0"}}

      :meck.expect(ManifestScroll, :insert, fn _ -> fake_manifest end)
      :meck.expect(DefaultEventEmitter, :fire_deployment_staged, fn _ -> :ok end)

      ^fake_manifest = PutDeployment.invoke(fake_manifest)

      :meck.wait(ManifestScroll, :insert, [fake_manifest], 1000)
      :meck.wait(DefaultEventEmitter, :fire_deployment_staged, [fake_manifest], 1000)
    end
  end
end
