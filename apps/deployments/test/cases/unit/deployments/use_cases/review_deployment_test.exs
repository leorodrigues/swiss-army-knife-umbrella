defmodule Deployments.UseCases.ReviewDeploymentTest do
  use ExUnit.Case

  alias Deployments.UseCases.ReviewDeployment
  alias Deployments.UseCases.Dependencies.TryFulfillingDeployment

  alias Deployments.State.ManifestScroll

  alias Deployments.Domain.AppKey
  alias Deployments.Domain.Manifest

  setup do
    :meck.new([TryFulfillingDeployment, ManifestScroll])
    :meck.new(Logger, [:passthrough])
    on_exit(fn -> :meck.unload end)
  end

  describe "Deployments.UseCases.ReviewDeployment.invoke" do
    test "should try fulfilling a deployment" do
      app_key = %AppKey{name: :app, version: "1.0.0"}
      manifest = %Manifest{app_key: app_key}

      :meck.expect(ManifestScroll, :find_by_app_key, fn _ -> manifest end)
      :meck.expect(TryFulfillingDeployment, :invoke, fn _ -> :ok end)

      :ok = ReviewDeployment.invoke(app_key)

      :meck.wait(ManifestScroll, :find_by_app_key, [app_key], 3000)
      :meck.wait(TryFulfillingDeployment, :invoke, [manifest], 3000)
    end

    test "should return :not_found if the manifest is not found" do
      app_key = %AppKey{name: :app, version: "1.0.0"}

      :meck.expect(ManifestScroll, :find_by_app_key, fn _ -> :not_found end)

      :not_found = ReviewDeployment.invoke(app_key)

      :meck.wait(ManifestScroll, :find_by_app_key, [app_key], 3000)
      :meck.wait(0, TryFulfillingDeployment, :invoke, :_, 3000)
    end
  end
end
