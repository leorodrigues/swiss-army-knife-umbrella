defmodule Deployments.DummyModule do
  def find_manifest_by_phase(_), do: [ ]
  def fire_deployment_confirmed(_), do: :ok
  def fire_deployment_application_failed(_), do: :ok
  def update_manifest(_), do: :ok
end
