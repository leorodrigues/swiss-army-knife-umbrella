defmodule Deployments.EmptyHandlers do

  require Logger

  def handle_get(t, i, p) do
    Logger.info(comment: "handle_get", topic: t, inbox: i, payload: p)
  end

  def handle_put(t, i, p) do
    Logger.info(comment: "handle_put", topic: t, inbox: i, payload: p)
  end

  def handle_staged(t, i, p) do
    Logger.info(comment: "handle_staged", topic: t, inbox: i, payload: p)
  end

  def handle_confirmed(t, i, p) do
    Logger.info(comment: "handle_confirmed", topic: t, inbox: i, payload: p)
  end
end
