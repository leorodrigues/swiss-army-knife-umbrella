defmodule Deployments.Interface.Inbound.SimpleUsageEventListener do
  use Hermes.Piping.Listener,
  supervisor: Deployments.Test.CommandTasker,
  pipeline: Hermes.Piping.Inbound.EventPipeline,
  inbox: "deployment-tests"

  alias Deployments.Interface.Topics

  import Deployments.EmptyHandlers

  listen_to Topics.deployment_staged_event, payload do
    handle_staged(Topics.deployment_staged_event, "deployment-tests", payload)
  end

  listen_to Topics.deployment_confirmed_event, payload do
    handle_confirmed(Topics.deployment_confirmed_event, "deployment-tests", payload)
  end
end
