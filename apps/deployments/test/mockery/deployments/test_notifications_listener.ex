defmodule Deployments.TestNotificationsListener do
  use Hermes.Piping.Listener,
    supervisor: Deployments.Test.CommandTasker,
    pipeline: Hermes.Piping.Inbound.VoidCommandPipeline,
    inbox: "deployment-tests"

  require Logger

  alias Deployments.Interface.Topics

  listen_to Topics.put_notification_command, payload do
    Logger.info(comment: "Handling notification", payload: payload)
  end
end
