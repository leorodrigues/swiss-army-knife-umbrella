defmodule Deployments.TestReplyListener do
  use Hermes.Piping.Listener,
    supervisor: Deployments.Test.CommandTasker,
    pipeline: Hermes.Piping.Inbound.CommandReplyPipeline,
    inbox: "deployment-tests"

  alias Deployments.TestTopics
  alias Deployments.EmptyHandlers

  import EmptyHandlers

  listen_to TestTopics.get_deployment_command_reply, payload do
    handle_get(TestTopics.get_deployment_command_reply, "deployment-tests", payload)
  end

  listen_to TestTopics.put_deployment_command_reply, payload do
    handle_put(TestTopics.put_deployment_command_reply, "deployment-tests", payload)
  end
end
