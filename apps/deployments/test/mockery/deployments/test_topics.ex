defmodule Deployments.TestTopics do

  import Hermes.DynamicTopics

  @app_name :deployments

  @caller_name :deployment_tests

  @domain "org.leonardo.sak"

  @get_deployment_command_reply compute_reply_topic(@domain, @app_name, @caller_name, :get)
  @spec get_deployment_command_reply :: String.t
  def get_deployment_command_reply, do: @get_deployment_command_reply

  @put_deployment_command_reply compute_reply_topic(@domain, @app_name, @caller_name, :put)
  @spec put_deployment_command_reply :: String.t
  def put_deployment_command_reply, do: @put_deployment_command_reply

end
