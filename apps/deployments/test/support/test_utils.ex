defmodule Deployments.TestUtils do
  def stack(name, elements) do
    table = :ets.new(name, [:set, :public])
    :ets.insert(table, {:stack, elements})
    table
  end

  def pop(table) do
    case :ets.lookup(table, :stack) do
      [{:stack, [ ]}] -> throw {:error, "Stack empty"}
      [{:stack, [head|tail]}] ->
        :ets.insert(table, {:stack, tail})
        head
    end
  end

  def load_samples(context, file_path) do
    with {:ok, contents} <- :file.consult(file_path) do
      Map.put(context, :samples, Enum.into(contents, %{ }))
    end
  end

  def wait_on(payload, module, function, topic, inbox, timeout) do
    :meck.wait(module, function, [topic, inbox, payload], timeout)
    payload
  end

  def ensure_call_count(payload, count, module, function, topic, inbox) do
    ^count = :meck.num_calls(module, function, [topic, inbox, payload])
    payload
  end

  def call_hermes(message, topic), do: Hermes.publish(topic, message)

  def submit_command(payload, reply_topic, topic) do
    Hermes.Piping.submit_command(reply_topic, topic, payload)
  end

  def using_sample(map, key) do
    case Map.get(map, key) do
      nil -> throw {:error, {:test_case_requirement, :sample_not_found, key}}
      value -> value
    end
  end

  def with_listeners(listeners, test_function) do
    Enum.each(listeners, fn listener -> listener.start_link() end)
    try do
      test_function.()
    after
      Enum.each(listeners, fn listener -> listener.stop() end)
    end
  end
end
