defmodule Gateway.Application do
  @moduledoc false

  use Application

  alias Gateway.Framework.Bootstrap

  alias Gateway.System.Activities.Tasker

  alias Gateway.System.Activities.BecomeReady
  alias Gateway.System.Activities.BecomeReadyInjection
  alias Gateway.System.Activities.DeploymentSequence

  alias Gateway.Interface.Outbound.ExternalCommands

  alias Gateway.Interface.Inbound.Phoenix.Server
  alias Gateway.Interface.Inbound.Phoenix.ServerInject
  alias Gateway.Interface.Inbound.CommandListener
  alias Gateway.Interface.Inbound.CommandTasker

  alias Gateway.MainDynamicSupervisor

  @impl true
  def start(_type, _args) do
    children = [
      {DynamicSupervisor, name: MainDynamicSupervisor, strategy: :one_for_one},

      {Task.Supervisor, name: CommandTasker},

      {Task.Supervisor, name: Tasker},

      Server.make_child_spec(%ServerInject{
        notify: &ExternalCommands.notify_info_message/2
      }),

      BecomeReady.make_child_spec(%BecomeReadyInjection{
        notify: &ExternalCommands.notify_info_message/1,
        start_accepting: &Server.start/0
      }),

      DeploymentSequence,

      CommandListener
    ]

    opts = [strategy: :one_for_one, name: Gateway.Supervisor]
    result = Supervisor.start_link(children, opts)

    Bootstrap.run()

    result
  end
end
