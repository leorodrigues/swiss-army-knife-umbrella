defmodule Gateway.Framework.Bootstrap do

  alias Gateway.System.Activities.DeploymentSequence

  require Logger

  @app_name :gateway

  @spec run :: :ok
  def run do
    unless Application.get_env(@app_name, :skip_self_deploy, false) do
      DeploymentSequence.begin()
    end
    :ok
  end
end
