defmodule Gateway.Interface.Inbound.CommandListener do
  use Hermes.Piping.Listener,
    supervisor: Gateway.Interface.Inbound.CommandTasker,
    pipeline: Hermes.Piping.Inbound.VoidCommandPipeline,
    inbox: "gateway"

  alias Gateway.Interface.Topics

  require Logger

  listen_to Topics.register_application_command, _ do
    Logger.info(comment: "pending implementation", function: :register_application)
  end

  listen_to Topics.unregister_application_command, _ do
    Logger.info(comment: "pending implementation", function: :unregister_application)
  end
end
