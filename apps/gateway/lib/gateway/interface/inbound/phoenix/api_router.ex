defmodule Gateway.Interface.Inbound.Phoenix.ApiRouter do
  use Plug.Router

  require Logger

  plug :match
  plug :dispatch

  post "/ping" do
    conn
      |> put_resp_content_type("application/json")
      |> send_resp(200, Poison.encode!(%{message: "pong"}))
  end
end
