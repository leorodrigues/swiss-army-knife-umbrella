defmodule Gateway.Interface.Inbound.Phoenix.Endpoint do
  use Plug.Router
  use Plug.Debugger
  use Plug.ErrorHandler

  require Logger

  plug Plug.RequestId
  plug Plug.Logger, log: :debug
  plug :match
  plug Plug.Parsers,
    parsers: [:json],
    pass: ["application/json"],
    json_decoder: Poison

  plug :dispatch

  forward "/api", to: Gateway.Interface.Inbound.Phoenix.ApiRouter

  match _ do
    send_resp conn, 404, "Not found"
  end

end
