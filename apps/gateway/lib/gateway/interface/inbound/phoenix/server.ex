defmodule Gateway.Interface.Inbound.Phoenix.Server do
  use GenServer

  alias Gateway.Interface.Inbound.Phoenix.ServerInject
  alias Gateway.Interface.Inbound.Phoenix.Endpoint

  alias Gateway.MainDynamicSupervisor

  alias Plug.Cowboy

  @default_port 8081

  @listening_message "Gateway listening for requests."

  @self __MODULE__

  def start do
    GenServer.call(@self, :start)
  end

  def make_child_spec(%ServerInject{ } = i) do
    {@self, init_args: [inject: i], name: @self}
  end

  def start_link(opts \\ [ ]) do
    args = Keyword.get(opts, :init_args, [ ])
    GenServer.start_link(@self, args, Keyword.put(opts, :name, @self))
  end

  @impl true
  def init(args \\ [ ]) do
    {:ok, Enum.into(args, %{ })}
  end

  @impl true
  def handle_call(:start, _from, state) do
    get_config() |> start_cowboy() |> notify(state)
    {:reply, :ok, state}
  end

  defp get_config do
    with {:ok, config} <- Application.fetch_env(:gateway, Endpoint) do
      Keyword.merge([port: @default_port], config)
    end
  end

  defp start_cowboy(config) do
    DynamicSupervisor.start_child(MainDynamicSupervisor, Cowboy.child_spec(
      plug: Endpoint, options: config, scheme: :http
    ))
    config
  end

  defp notify(config, %{inject: %{notify: f}}) do
    with [port: port] <- config do
      f.(@listening_message, %{
        port: port, address: "localhost", protocol: "http"
      })
    end
  end
end
