defmodule Gateway.Interface.Inbound.Phoenix.ServerInject do
  defstruct [notify: nil]

  @type notify :: (String.t, map -> :ok) | nil

  @type t :: %__MODULE__{notify: notify}
end
