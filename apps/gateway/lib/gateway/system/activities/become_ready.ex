defmodule Gateway.System.Activities.BecomeReady do

  alias CommonActivities.Basic.ContextualServer

  alias Gateway.System.Activities.Tasker

  alias Gateway.System.Activities.BecomeReadyInjection

  use ContextualServer, supervisor: Tasker

  require Logger

  @self __MODULE__

  # Business interface ---------------------------------------------------------

  def invoke do
    ContextualServer.call(@self, :invoke)
  end

  # GenServer interface --------------------------------------------------------

  @spec make_child_spec(injection :: BecomeReadyInjection.t) :: {atom, keyword}
  def make_child_spec(%BecomeReadyInjection{ } = i) do
    {@self, init_args: [injection: i], name: @self}
  end

  def start_link(opts \\ [ ]) do
    args = Keyword.get(opts, :init_args, [ ])
    GenServer.start_link(@self, args, name: @self)
  end

  @impl true
  def init(args \\ [ ]) do
    {:ok, Enum.into(args, %{ })}
  end

  @impl true
  def handle_contextual_call(:invoke, _from, state) do
    start_accepting_requests(state)
    notify_ready(state)
    {:reply, :ok, state}
  end

  defp start_accepting_requests(%{injection: i}) do
    with %BecomeReadyInjection{start_accepting: f} <- i, do: f.()
  end

  defp notify_ready(%{injection: i}) do
    with %BecomeReadyInjection{notify: f} <- i, do: f.("Gateway ready")
  end
end
