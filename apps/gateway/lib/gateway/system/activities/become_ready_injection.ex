defmodule Gateway.System.Activities.BecomeReadyInjection do
  defstruct [:notify, :start_accepting]

  @type t :: %__MODULE__{
    notify: (map -> :ok) | nil,
    start_accepting: (-> :ok) | nil
  }
end
