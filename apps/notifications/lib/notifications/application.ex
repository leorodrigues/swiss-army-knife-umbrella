defmodule Notifications.Application do
  @moduledoc false

  use Application

  alias Notifications.Interface.Inbound.CommandListener
  alias Notifications.Interface.Console.Printer

  alias Notifications.System.Activities.NotifyLocally
  alias Notifications.System.Activities.DeploymentSequence
  alias Notifications.System.Activities.BecomeReady
  alias Notifications.System.Activities.BecomeReadyInjection

  alias Notifications.UseCases.PutNotification
  alias Notifications.UseCases.PutNotificationInject

  alias Notifications.Framework.Bootstrap

  @impl true
  def start(_type, _args) do
    children = [
      {Task.Supervisor, name: Notifications.Interface.CommandTasker},

      {Task.Supervisor, name: Notifications.System.Activities.Tasker},

      {Task.Supervisor, name: Notifications.UseCases.Tasker},

      PutNotification.make_child_spec(%PutNotificationInject{
        print_info: &Printer.print_info/1,
        print_error: &Printer.print_error/1,
        print_warning: &Printer.print_warning/1
      }),

      BecomeReady.make_child_spec(%BecomeReadyInjection{
        notify: &NotifyLocally.invoke/1
      }),

      DeploymentSequence,

      CommandListener
    ]

    opts = [strategy: :one_for_one, name: Notifications.Supervisor]
    result = Supervisor.start_link(children, opts)

    Bootstrap.run()

    result
  end
end
