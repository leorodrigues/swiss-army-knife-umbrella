defmodule Notifications.Domain.Clock do

  @spec timestamp :: non_neg_integer
  def timestamp, do: System.system_time(:microsecond)
end
