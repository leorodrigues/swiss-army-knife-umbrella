defmodule Notifications.Domain.Notification do

  alias Notifications.Domain.Clock

  @display [:meta, :token, :message, :created_at, :timestamp]

  defstruct [
    id: nil,
    level: :info,
    token: nil,
    message: nil,
    timestamp: nil,
    meta: nil,
    created_at: nil
  ]

  @type t :: %__MODULE__{
    id: String.t | nil,
    level: :info | :warn | :error | nil,
    token: String.t | nil,
    message: String.t | nil,
    timestamp: Clock.timestamp | nil,
    meta: any | nil,
    created_at: Clock.timestamp | nil
  }

  @spec new_from_map(map) :: __MODULE__.t
  def new_from_map(%{ } = map) do
    %__MODULE__{
      id: UUID.uuid4(),
      level: map[:level],
      token: map[:token],
      message: map[:message],
      timestamp: map[:timestamp],
      meta: map[:meta],
      created_at: Clock.timestamp
    }
  end

  def to_line(%__MODULE__{ } = notification) do
    chunk_fun = fn key, line ->
      case Map.get(notification, key) do
        nil -> {:cont, line}
        value -> {:cont, [{key, value}|line]}
      end
    end

    after_fun = fn l -> {:cont, l, nil} end

    @display |> Enum.chunk_while([ ], chunk_fun, after_fun) |> head()
  end

  defp head([head|_]), do: head
  defp head([]), do: [ ]
end
