defmodule Notifications.Framework.Bootstrap do

  alias Notifications.System.Activities.DeploymentSequence

  require Logger

  @app_name :notifications

  @comment "Boot strapping"

  def run do
    Logger.debug([comment: @comment, app: @app_name])
    unless Application.get_env(@app_name, :skip_self_deploy, false) do
      DeploymentSequence.begin()
    end
    :ok
  end
end
