defmodule Notifications.Interface.Console.Printer do

  require Logger

  def print_info(line), do: format_line(line) |> Logger.info()

  def print_error(line), do: format_line(line) |> Logger.error()

  def print_warning(line), do: format_line(line) |> Logger.warning()

  defp format_line(line), do: format_line(line, [ ])

  defp format_line([ ], result), do: Enum.reverse(result)

  defp format_line([{:timestamp = k, value}|tail], result) do
    format_line(tail, [{k, format_time_stamp(value)}|result])
  end

  defp format_line([{:created_at = k, value}|tail], result) do
    format_line(tail, [{k, format_time_stamp(value)}|result])
  end

  defp format_line([{:meta = k, value}|tail], result) do
    format_line(tail, [{k, format_map(value)}|result])
  end

  defp format_line([head|tail], result), do: format_line(tail, [head|result])

  defp format_map(%{ } = map) do
    Map.keys(map) |> Enum.reduce(map, fn k, m ->
      case k do
        :created_at -> Map.put(m, k, format_time_stamp(Map.get(m, k)))
        :updated_at -> Map.put(m, k, format_time_stamp(Map.get(m, k)))
        :timestamp -> Map.put(m, k, format_time_stamp(Map.get(m, k)))
        _ -> m
      end
    end)
  end

  def format_time_stamp(nil), do: nil
  def format_time_stamp(timestamp) do
    :calendar.system_time_to_rfc3339(timestamp, [unit: :microsecond])
      |> to_string()
  end

end
