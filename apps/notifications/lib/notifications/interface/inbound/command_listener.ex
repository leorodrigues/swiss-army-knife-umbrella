defmodule Notifications.Interface.Inbound.CommandListener do
  use Hermes.Piping.Listener,
    supervisor: Notifications.Interface.CommandTasker,
    pipeline: Hermes.Piping.Inbound.VoidCommandPipeline,
    inbox: "notifications"

    alias Notifications.UseCases.PutNotification

    alias Notifications.Interface.Topics

    import Notifications.Interface.PayloadMapping

    import :church

    require Logger

    listen_to Topics.put_notification_command, payload do
      chain_apply(payload, [
        &to_notification/1,
        &break_on_error/1,
        &PutNotification.invoke/1
      ]) |> report_result()
    end

    defp report_result({:error, _} = e) do
      Logger.error(comment: "Failed to register notification.", error: e)
    end

    defp report_result(_) do
      Logger.debug(comment: "Successfuly registered a notification.")
    end
end
