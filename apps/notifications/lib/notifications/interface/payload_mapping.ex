defmodule Notifications.Interface.PayloadMapping do

  alias Notifications.Domain.Notification

  @spec to_notification(map :: %{ }) :: Notification.t
  def to_notification(%{ } = map) do
    Notification.new_from_map(map)
  end
end
