defmodule Notifications.Interface.Topics do

  import Hermes.DynamicTopics

  @app_name :notifications
  
  @domain "org.leonardo.sak"

  @get_deployment_reply compute_reply_topic(@domain, :deployments, @app_name, :get)
  @spec get_deployment_reply :: String.t
  def get_deployment_reply, do: @get_deployment_reply

  @put_deployment_reply compute_reply_topic(@domain, :deployments, @app_name, :put)
  @spec put_deployment_reply :: String.t
  def put_deployment_reply, do: @put_deployment_reply

  @put_notification_command compute_command_topic(@domain, @app_name, :put)
  @spec put_notification_command :: String.t
  def put_notification_command, do: @put_notification_command
end
