defmodule Notifications.System.Activities.BecomeReadyInjection do
  defstruct [:notify]

  @type t :: %__MODULE__{
    notify: (map -> :ok) | nil
  }
end
