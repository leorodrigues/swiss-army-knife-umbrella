defmodule Notifications.System.Activities.DeploymentSequence do

  alias Notifications.System.Activities.Tasker

  alias Notifications.System.Activities.BecomeReady

  alias Notifications.Interface.Topics

  use CommonActivities.Deployment.DeploymentSequence,
    get_deployment_reply_topic: Topics.get_deployment_reply,
    put_deployment_reply_topic: Topics.put_deployment_reply,
    supervisor: Tasker,
    inbox: "notifications"

  @app_name :notifications

  @impl true
  @spec get_app_key :: map
  def get_app_key, do: %{ app_name: @app_name, app_version: "1.0.0" }

  @impl true
  @spec get_app_manifest :: map
  def get_app_manifest, do: %{
    app_key: get_app_key(),
    dependencies: [
      %{ app_name: :deployments, app_version: "1.0.0" }
    ]
  }

  @impl true
  def on_deployment_succeeded do
    BecomeReady.invoke
  end

  @impl true
  def on_already_deployed do
    BecomeReady.invoke
  end
end
