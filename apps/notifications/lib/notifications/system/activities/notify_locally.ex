defmodule Notifications.System.Activities.NotifyLocally do

  alias Notifications.Domain.Clock
  alias Notifications.Domain.Notification
  alias Notifications.UseCases.PutNotification

  def invoke(content) do
    PutNotification.invoke(%Notification{
      level: :info,
      message: content,
      timestamp: Clock.timestamp()
    })
  end
end
