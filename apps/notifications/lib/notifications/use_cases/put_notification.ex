defmodule Notifications.UseCases.PutNotification do

  alias CommonActivities.Basic.ContextualServer

  alias Notifications.UseCases.PutNotificationInject
  alias Notifications.UseCases.Tasker

  alias Notifications.Domain.Notification

  use ContextualServer, supervisor: Tasker

  require Logger

  @self __MODULE__

  @spec invoke(notification :: Notification.t) :: Notification.t
  def invoke(%Notification{ } = n) do
    ContextualServer.call(@self, {:print, n})
  end

  def make_child_spec(%PutNotificationInject{ } = i) do
    {@self, [init_args: [inject: i]]}
  end

  def start_link(opts \\ [ ]) do
    args = Keyword.get(opts, :init_args, [ ])
    GenServer.start_link(@self, args, Keyword.put(opts, :name, @self))
  end

  @impl true
  def init(args \\ [ ]) do
    {:ok, Enum.into(args, %{ })}
  end

  @impl true
  def handle_contextual_call({:print, n}, _from, %{inject: i} = state) do
    select_printer(n, i) |> invoke_printer(Notification.to_line(n))
    {:reply, :ok, state}
  end

  defp select_printer(%Notification{level: :info}, %{print_info: f}), do: f

  defp select_printer(%Notification{level: :error}, %{print_error: f}), do: f

  defp select_printer(%Notification{level: :warn}, %{print_warning: f}), do: f

  defp invoke_printer(function, line), do: function.(line)
end
