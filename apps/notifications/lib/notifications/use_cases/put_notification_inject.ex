defmodule Notifications.UseCases.PutNotificationInject do
  defstruct [:print_info, :print_error, :print_warning]

  @type printer_function :: (keyword -> :ok)

  @type t :: %__MODULE__{
    print_info: printer_function,
    print_error: printer_function,
    print_warning: printer_function
  }
end
