defmodule Notifications.IntegrationTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :notifications

  alias Notifications.UseCases.PutNotification

  alias Notifications.Interface.Inbound.CommandListener

  alias Notifications.Interface.Topics

  alias Notifications.Interface.CommandTasker

  import Notifications.TestUtils

  @modules_to_spy_on [PutNotification]

  @test_listeners [CommandListener]

  setup_all_environment do
    Hermes.set_dispatch_on_publish(:single_topic)
    [ ]
  end

  setup ctx do
    case ctx do
      %{samples_file: f} -> load_samples(ctx, resolve_test_resource_path([f]))
      _ -> :ok
    end
  end

  setup do
    :meck.new(@modules_to_spy_on, [:passthrough])

    Task.Supervisor.start_link(name: CommandTasker)

    Application.put_env(:notifications, :skip_self_deploy, false)
    on_exit(fn ->
      Application.get_env(:notifications, :skip_self_deploy, true)
      :meck.unload
    end)
  end

  describe "Notifications" do
    @tag samples_file: "case1.terms"
    test "run deployment procedures", %{samples: samples} do
      with_listeners(@test_listeners, fn ->
        %{notification_payload: p, notification_instance: i} = samples

        Hermes.Piping.submit_void_command(Topics.put_notification_command, p)

        :meck.wait(PutNotification, :invoke, [i], 2000)
      end)
    end
  end
end
