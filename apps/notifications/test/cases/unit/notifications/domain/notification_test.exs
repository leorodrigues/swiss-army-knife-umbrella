defmodule Notifications.Domain.NotificationTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :notifications

  alias Notifications.Domain.Clock
  alias Notifications.Domain.Notification

  import Notifications.TestUtils

  @modules_to_stub [Clock, UUID]

  setup_all_environment do
    :ok
  end

  setup ctx do
    case ctx do
      %{samples_file: f} -> load_samples(ctx, resolve_test_resource_path([f]))
      _ -> :ok
    end
  end

  setup do
    :meck.new(@modules_to_stub)
    on_exit(fn -> :meck.unload end)
  end

  describe "Notifications.Domain.Notification.to_line" do

    @tag samples_file: "case7.terms"
    test "should turn a notification instance into a log line", %{samples: s} do
      %{log_line: l, notification: n} = s
      ^l = Notification.to_line(n)
    end

    @tag samples_file: "case7.terms"
    test "should omit nil attributes on a log line", %{samples: s} do
      %{log_line: l, notification: n} = s
      ^l = Notification.to_line(n)
    end
  end

  describe "Notifications.Domain.Notification.new_from_map" do
    @tag samples_file: "case1.terms"
    test "should create an empty instance", %{samples: s} do
      with %{id: i, created_at: c, notification_map: m, notification_instance: n} <- s do
        :meck.expect(UUID, :uuid4, fn -> i end)
        :meck.expect(Clock, :timestamp, fn -> c end)

        ^n = Notification.new_from_map(m)
      end
    end

    @tag samples_file: "case2.terms"
    test "should create an instance with a message", %{samples: s} do
      with %{id: i, created_at: c, notification_map: m, notification_instance: n} <- s do
        :meck.expect(UUID, :uuid4, fn -> i end)
        :meck.expect(Clock, :timestamp, fn -> c end)

        ^n = Notification.new_from_map(m)
      end
    end

    @tag samples_file: "case3.terms"
    test "should create an instance with a timestamp", %{samples: s} do
      with %{id: i, created_at: c, notification_map: m, notification_instance: n} <- s do
        :meck.expect(UUID, :uuid4, fn -> i end)
        :meck.expect(Clock, :timestamp, fn -> c end)

        ^n = Notification.new_from_map(m)
      end
    end

    @tag samples_file: "case4.terms"
    test "should create an instance with a level", %{samples: s} do
      with %{id: i, created_at: c, notification_map: m, notification_instance: n} <- s do
        :meck.expect(UUID, :uuid4, fn -> i end)
        :meck.expect(Clock, :timestamp, fn -> c end)

        ^n = Notification.new_from_map(m)
      end
    end

    @tag samples_file: "case5.terms"
    test "should create an instance with a token", %{samples: s} do
      with %{id: i, created_at: c, notification_map: m, notification_instance: n} <- s do
        :meck.expect(UUID, :uuid4, fn -> i end)
        :meck.expect(Clock, :timestamp, fn -> c end)

        ^n = Notification.new_from_map(m)
      end
    end

    @tag samples_file: "case6.terms"
    test "should create an instance with meta information", %{samples: s} do
      with %{id: i, created_at: c, notification_map: m, notification_instance: n} <- s do
        :meck.expect(UUID, :uuid4, fn -> i end)
        :meck.expect(Clock, :timestamp, fn -> c end)

        ^n = Notification.new_from_map(m)
      end
    end
  end
end
