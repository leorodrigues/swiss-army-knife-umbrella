defmodule Notifications.Framework.BootstrapTest do
  use ExUnit.Case

  alias Notifications.Framework.Bootstrap
  alias Notifications.System.Activities.DeploymentSequence

  setup do
    :meck.new(DeploymentSequence)
    on_exit(fn -> :meck.unload end)
  end

  describe "Notifications.Framework.Bootstrap.run" do
    test "should subscribe to callbacks and issue a get_deployment command" do
      Application.put_env(:notifications, :skip_self_deploy, false)

      :meck.expect(DeploymentSequence, :begin, fn -> :ok end)

      :ok = Bootstrap.run()

      :meck.wait(DeploymentSequence, :begin, [], 3000)
    end
  end
end
