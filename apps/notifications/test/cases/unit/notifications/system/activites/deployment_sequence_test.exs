defmodule Notifications.System.Activities.DeploymentSequenceTest do
  use ExUnit.Case

  alias Notifications.System.Activities.Tasker

  alias Notifications.System.Activities.DeploymentSequence
  alias Notifications.System.Activities.BecomeReady

  alias Hermes.Piping

  setup do
    Application.put_env(:notifications, :skip_self_deploy, false)

    Task.Supervisor.start_link(name: Tasker)

    :meck.new([BecomeReady, Piping])
    on_exit(fn -> :meck.unload end)
  end

  describe "Notifications.System.Activities.DeploymentSequence.continue" do
    test "should become ready if the deployment is marked as deployed" do
      :meck.expect(BecomeReady, :invoke, fn -> :ok end)

      DeploymentSequence.continue(%{phase: :deployed})

      :meck.wait(BecomeReady, :invoke, [ ], 1000)
    end
  end

  describe "Notifications.System.Activities.DeploymentSequence.finish" do
    test "should become ready if the deployment is marked as deployed" do
      :meck.expect(BecomeReady, :invoke, fn -> :ok end)

      DeploymentSequence.finish(%{phase: :deployed})

      :meck.wait(BecomeReady, :invoke, [ ], 1000)
    end
  end
end
