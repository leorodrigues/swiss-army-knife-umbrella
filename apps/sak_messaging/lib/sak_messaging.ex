defmodule SakMessaging do

  @separator "/"

  import Enum, only: [join: 2]

  @spec compute_event_topic(
    app_name :: atom, event_name :: String.t | atom
  ) :: String.t
  def compute_event_topic(app_name, event_name) do
    [base_path(app_name, "events"), dashify(event_name)]
      |> join(@separator)
  end

  @spec compute_command_topic(
    app_name :: atom, command_name :: String.t | atom
  ) :: String.t
  def compute_command_topic(app_name, command_name) do
    [base_path(app_name, "commands"), dashify(command_name)]
      |> join(@separator)
  end

  @spec compute_callback_topic(
    app_name :: atom, target_name :: atom, command_name :: String.t | atom
  ) :: String.t
  def compute_callback_topic(app_name, target_name, cmd_name) do
    [base_path(app_name, "callbacks"), dashify(target_name), dashify(cmd_name)]
      |> join(@separator)
  end

  defp base_path(app_name, type) do
    ["topics", "sak", dashify(app_name), type] |> join(@separator)
  end

  defp dashify(input) when is_atom(input), do: to_string(input) |> dashify()
  defp dashify(input) when is_binary(input), do: String.replace(input, "_", "-")
end
