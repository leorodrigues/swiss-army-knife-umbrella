defmodule SakMessaging.CallbackRecords do
  @type reply_context :: { topic_id :: String.t, headers :: %{ } } | String.t

  @callback insert(key :: term, context :: reply_context) :: :ok
  @callback remove_by_key(key :: term) :: :ok
  @callback find_by_key(key :: term) :: String.t
end
