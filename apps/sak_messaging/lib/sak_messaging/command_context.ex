defmodule SakMessaging.CommandContext do

  alias SakMessaging.CommandContextServer

  @spec make_child_spec(any, any) :: {atom, keyword}
  def make_child_spec(server_name, records_module) do
    {CommandContextServer, name: server_name, init_args: [
      context_records: records_module
    ]}
  end

  def hold(server_name, id) do
    CommandContextServer.hold(server_name, id)
  end

  def drop(server_name, id) do
    CommandContextServer.drop(server_name, id)
  end

  def load(server_name, id) do
    CommandContextServer.load(server_name, id)
  end
end
