defmodule SakMessaging.CommandContextRecords do
  @type command_context :: %{ }

  @callback insert(key :: term, context :: command_context) :: String.t
  @callback remove_by_key(key :: term) :: String.t
  @callback find_by_key(key :: term) :: command_context | :not_found
end
