defmodule SakMessaging.CommandContextServer do
  use GenServer

  alias Hermes.Domain.Context

  import :church

  require Logger

  def hold(server_name, id) do
    GenServer.call(server_name, {:hold_context, id, get_process_context()})
  end

  def drop(server_name, id) do
    GenServer.call(server_name, {:drop_context, id})
  end

  def load(server_name, id) do
    GenServer.call(server_name, {:load_context, id}) |> restore_context()
  end

  def start_link(opts \\ [ ]) do
    GenServer.start_link(__MODULE__, confirm_init_args(opts), confirm_opts(opts))
  end

  @impl true
  def init(args \\ [ ]) do
    {:ok, Enum.into(args, %{ })}
  end

  @impl true
  def handle_call({:hold_context, id, context}, _from, state) do
    handle_hold_context(id, context, state)
  end

  @impl true
  def handle_call({:drop_context, id}, _from, state) do
    handle_drop_context(id, state)
  end

  @impl true
  def handle_call({:load_context, id}, _from, state) do
    handle_load_context(id, state)
  end

  defp handle_hold_context(id, context, %{context_records: records} = state) do
    records.insert(id, context)
    {:reply, :ok, state}
  end

  defp handle_drop_context(id, %{context_records: records} = state) do
    records.remove_by_key(id)
    {:reply, :ok, state}
  end

  defp handle_load_context(id, %{context_records: records} = state) do
    {:reply, records.find_by_key(id), state}
  end

  defp confirm_init_args(opts) do
    chain_apply(Keyword.get(opts, :init_args, [ ]), [
      partial_left(&confirm_key/2, [:context_records])
    ])
  end

  defp confirm_key(opts, key) do
    case Keyword.has_key?(opts, key) do
      false -> throw {:error, {:init_arg_required, key}}
      true -> opts
    end
  end

  defp confirm_opts(opts) do
    case Keyword.has_key?(opts, :name) do
      false -> throw {:error, {:option_required, :name}}
      true -> opts
    end
  end

  defp restore_context(:not_found), do: put_process_context(%{ })
  defp restore_context(%{ } = context), do: put_process_context(context)

  def put_process_context(map) do
    Process.put(Context.context_name, map)
  end

  def get_process_context do
    Process.get(Context.context_name, %{ })
  end
end
