defmodule SakMessaging.LateReplyCommandPipeline do
  use Hermes.Piping.Pipe

  alias SakMessaging.CommandContext

  require Logger

  pipe Hermes.Piping.Inbound.OpenCommandPipe
  pipe Hermes.Piping.Inbound.RunBusinessTargetPipe
  pipe :hold_context

  defp hold_context(id, opts) do
    case Keyword.get(opts, :state) do
      %{context_server_name: n} -> CommandContext.hold(n, id)
      nil -> Logger.warning(comment: "Unknown opts", opts: opts)
    end
  end
end
