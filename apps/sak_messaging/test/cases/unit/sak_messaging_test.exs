defmodule SakMessagingTest do
  use ExUnit.Case

  import SakMessaging, only: [
    compute_event_topic: 2,
    compute_command_topic: 2,
    compute_callback_topic: 3
  ]

  describe "SakMessaging.compute_event_topic" do
    test "should return a topic binary" do
      topic = "topics/sak/some-app/events/something-happened"
      ^topic = compute_event_topic(:some_app, "something_happened")
    end
  end

  describe "SakMessaging.compute_command_topic" do
    test "should return a topic binary" do
      topic = "topics/sak/some-app/commands/do-something"
      ^topic = compute_command_topic(:some_app, "do_something")
    end
  end

  describe "SakMessaging.compute_callback_topic" do
    test "should return a topic binary" do
      topic = "topics/sak/some-app/callbacks/target-app/do-something"
      ^topic = compute_callback_topic(:some_app, :target_app, "do_something")
    end
  end
end
