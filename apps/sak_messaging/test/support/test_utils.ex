defmodule SakMessaging.TestUtils do
  def stack(name, elements) do
    table = :ets.new(name, [:set, :public])
    :ets.insert(table, {:stack, elements})
    table
  end

  def pop(table) do
    case :ets.lookup(table, :stack) do
      [{:stack, [ ]}] -> throw {:error, "Stack empty"}
      [{:stack, [head|tail]}] ->
        :ets.insert(table, {:stack, tail})
        head
    end
  end

  def load_samples(context, file_name) do
    with {:ok, contents} <- :file.consult(file_name) do
      Map.put(context, :samples, Enum.into(contents, %{ }))
    end
  end
end
