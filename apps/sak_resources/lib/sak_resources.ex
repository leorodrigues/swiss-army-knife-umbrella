defmodule SakResources do
  import Application, only: [app_dir: 2]

  def get_resource_path(:hermes, :schema_bootstrap_file) do
    app_dir(:sak_resources, ["priv", "hermes", "schema.terms"])
  end

  def get_resource_path(:hermes, :config_bootstrap_file) do
    app_dir(:sak_resources, ["priv", "hermes", "configuration.terms"])
  end
end
