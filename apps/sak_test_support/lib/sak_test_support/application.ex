defmodule SakTestSupport.Application do
  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # {SakTestSupport.Worker, arg}
    ]

    opts = [strategy: :one_for_one, name: SakTestSupport.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
