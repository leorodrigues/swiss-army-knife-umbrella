defmodule SwissArmyKnife.MixProject do
  use Mix.Project

  def project do
    [
      app: :swiss_army_knife,
      version: "1.0.0-a",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.15",
      elixirc_paths: elixirc_paths(Mix.env()),
      test_paths: test_paths(),
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      aliases: aliases(),
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.html": :test,
        "coveralls.unit": :test,
"coveralls.proto": :test,
        integration: :test,
        unit: :test,
        proto: :test,
        regression: :test
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :scrolls, :hermes],
      mod: {SwissArmyKnife.Application, []}
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support", "test/mockery", "prototyping"]
  defp elixirc_paths(:dev), do: ["lib", "test/mockery", "prototyping"]
  defp elixirc_paths(_), do: ["lib"]

  defp test_paths do
    ["test/cases/unit", "test/cases/integration", "test/cases/regression"]
  end

  # Run "mix help deps" to learn about dependencies.
    defp deps do
    [
      {:logger_file_backend, "~> 0.0.13"},
      {:sak_test_support, only: [:test], in_umbrella: true},
      {:dialyxir, "~> 1.3", only: [:dev], runtime: false},
      {:excoveralls, "~> 0.14.4", only: [:test]}
    ]
  end

    defp aliases do
    [
      integration: ["test test/cases/integration"],
      unit: ["test test/cases/unit"],
      regression: ["test test/cases/regression"],
      proto: ["test --only prototype:true"],
      "coveralls.unit": ["coveralls.html test/cases/unit"],
      "coveralls.proto": ["coveralls.html --only prototype:true"]
    ]
  end
end
