defmodule SwissArmyKnife.ManuallyBootstrapHermes do
  require Logger

  def invoke(ctx) do
    Logger.info([msg: "Started apps", list: Application.started_applications()])
    ctx
  end
end
