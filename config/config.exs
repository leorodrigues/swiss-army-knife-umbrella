import Config

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [
    :node, :application, :module, :pid,
    :message_type, :message_id, :correlation_id, :source_id
  ]

config :gateway, Gateway.Interface.Inbound.Phoenix.Endpoint, port: 8081

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
