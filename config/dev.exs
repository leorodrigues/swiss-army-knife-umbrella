import Config

config :logger, backends: [
  :console,
  {LoggerFileBackend, :full_log},
  {LoggerFileBackend, :hermes_log},
  {LoggerFileBackend, :notifications_log},
  {LoggerFileBackend, :gateway_log},
  {LoggerFileBackend, :core_log},
  {LoggerFileBackend, :deployments_log},
  {LoggerFileBackend, :common_activities_log},
  {LoggerFileBackend, :sak_messaging_log}
]

config :logger, :console,
  format: "$time $node $metadata[$level] $message\n",
  metadata: [:application],
  level: :info

config :logger, :full_log,
  path: "logs/full-output.log",
  format: "$time $node $metadata[$level] $message\n",
  metadata: [
    :node, :module, :pid, :message_type, :message_id,
    :correlation_id, :source_id
  ]

config :logger, :hermes_log,
  path: "logs/hermes.log",
  format: "$time $node $metadata[$level] $message\n",
  metadata_filter: [application: :hermes],
  metadata: [
    :node, :module, :pid, :message_type, :message_id,
    :correlation_id, :source_id
  ]

config :logger, :notifications_log,
  path: "logs/notifications.log",
  format: "$time $node $metadata[$level] $message\n",
  metadata_filter: [application: :notifications],
  metadata: [
    :node, :module, :pid, :message_type, :message_id,
    :correlation_id, :source_id
  ]

config :logger, :gateway_log,
  path: "logs/gateway.log",
  format: "$time $node $metadata[$level] $message\n",
  metadata_filter: [application: :gateway],
  metadata: [
    :node, :module, :pid, :message_type, :message_id,
    :correlation_id, :source_id
  ]

config :logger, :core_log,
  path: "logs/core.log",
  format: "$time $node $metadata[$level] $message\n",
  metadata_filter: [application: :core],
  metadata: [
    :node, :module, :pid, :message_type, :message_id,
    :correlation_id, :source_id
  ]

config :logger, :common_activities_log,
  path: "logs/common_activities.log",
  format: "$time $node $metadata[$level] $message\n",
  metadata_filter: [application: :common_activities],
  metadata: [
    :node, :module, :pid, :message_type, :message_id,
    :correlation_id, :source_id
  ]

config :logger, :deployments_log,
  path: "logs/deployments.log",
  format: "$time $node $metadata[$level] $message\n",
  metadata_filter: [application: :deployments],
  metadata: [
    :node, :module, :pid, :message_type, :message_id,
    :correlation_id, :source_id
  ]

config :logger, :sak_messaging_log,
  path: "logs/sak_messaging.log",
  format: "$time $node $metadata[$level] $message\n",
  metadata_filter: [application: :sak_messaging],
  metadata: [
    :node, :module, :pid, :message_type, :message_id,
    :correlation_id, :source_id
  ]

# config :git_hooks,
#   auto_install: :true,
#   verbose: true,
#   hooks: [
#     pre_push: [
#       tasks: [
#         {:cmd, "mix clean"},
#         {:cmd, "mix compile --warnings-as-errors"},
#         {:cmd, "mix dialyzer"},
#         {:cmd, "mix cmd --app core mix unit"},
#         {:cmd, "mix cmd --app notifications mix unit"},
#         {:cmd, "mix cmd --app deployments mix unit"},
#       ]
#     ]
#   ]
