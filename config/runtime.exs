import Config

mix_env = System.get_env("MIX_ENV", "dev")

user_home_path = System.user_home()

os_basedir = System.get_env("SAK_BASE_DATA_DIR", user_home_path)

base_data_dir = Path.join([os_basedir, ".sak", mix_env])

config :hermes, :base_dir, base_data_dir

config :hermes, :scrolls_base_data_dir, base_data_dir

config :swiss_army_knife, :scrolls_base_data_dir, base_data_dir

config :deployments, :scrolls_base_data_dir, base_data_dir

config :gateway, :scrolls_base_data_dir, base_data_dir

config :notifications, :scrolls_base_data_dir, base_data_dir

config :core, :scrolls_base_data_dir, base_data_dir

config :hermes, :schema_bootstrap_file,
  SakResources.get_resource_path(:hermes, :schema_bootstrap_file)

config :hermes, :config_bootstrap_file,
  SakResources.get_resource_path(:hermes, :config_bootstrap_file)

config :logger, level: String.to_atom(System.get_env("SAK_LOG_LEVEL", "info"))
