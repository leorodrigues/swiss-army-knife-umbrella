import Config

config :ex_test_support, :under_umbrella, true

config :notifications, :skip_self_deploy, true

config :core, :skip_self_deploy, true

config :gateway, :skip_self_deploy, true

config :deployments, :skip_self_deploy, true

config :hermes, env_provider: ExTestSupport.EnvProviderMock

config :hermes, dispatch_on_publish: :offline

config :scrolls, env_provider: ExTestSupport.EnvProviderMock

config :ex_test_support, ExTestSupport.EnvProviderMock,
  mock_for_get_env: {SakTestSupport.TestEnvProvider, :get_env}

config :gateway, Gateway.Interface.Inbound.Phoenix.Endpoint, port: 3001
