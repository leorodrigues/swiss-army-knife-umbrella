# Application Layering

## Basic Layer Specification

Applications are internally layered according to Domain Driven Design principles.
Each layer produces a package name under the application main package.
Note however, that the layer names used here are not the same as the ones used
by Eric Evans in his 2003 book. The following table translates Eric's naming
to our implementation:

| DDD            | SAK       | Reasoning                                                                            |
| ---            | ---       | ---                                                                                  |
| Domain         | Domain    |                                                                                      | 
| Application    | Use Cases | `Application` is a term already taken by the OTP framework                           |
| Infrastructure | System    | `System` is much shorter than `Infrastructure`, thus making code lines more succinct |
| Interface      | Interface |                                                                                      |

Now, suppose there is an application called _Accounting_ under an umbrella. The directory
structure would look like this:

```
apps/
|-- accounting
    |-- lib
        |-- accounting
            |-- domain
                |-- use_cases
                |-- system
                |-- interface
```

The layers are created so the that the business logic is protected from
expurious dependencies. For that reason, they have a precedence as displayed
below:

1. Domain
2. Use cases
3. System
4. Interface

The lower the number the higher is the precedence. A component sitting on any
layer is allowed to depend on components on the same layer or on a higher
precedence layer. So `[4]` may depend on `[3]`, `[2]`, or `[1]`;
`[2]` may depend only on `[1]`; and `[1]` may depend only on itself.

```txt
         + <== Precedence ==> -

[ Domain, Use Cases, System, Interface ]

[ <=================== Dependency flow ]
```

## Extended Layer Specification

The given layout is a general schema that is applied through all the project.
Some applications may need to define intermediary layers as needed.
Generally they will show up after layer 2. One such layer is the `State` layer,
housing the persistence logic as demonstrated below:

1. Domain
2. Use cases
3. System
4. State
5. Interface

Moreover, we use a final layer which is supposed to be the outermost layer in
any application, it is the `Framework` layer. The word **framework** here is
synonym for **chassis**, or **structure**. It is not a reference to a software
framework for a specific purpose, instead it's meant to signal that any
"connective" logic that an application needs should go in this layer. As this
is the final layer, anything new should come before it, nothing is allowed to
exist outside of it, which makes the following layer precedence list:

1. Domain
2. Use Cases
3. System
4. State
5. Interface
6. Framework

