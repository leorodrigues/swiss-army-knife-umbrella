defmodule SwissArmyKnife.Umbrella.MixProject do
  use Mix.Project

  def project do
    [
      apps_path: "apps",
      version: "1.0.0-beta",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      aliases: aliases(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.html": :test,
        integration: :test,
        unit: :test
      ],
      default_release: :standalone,
      releases: [
        standalone_mac_os: [
          steps: [:assemble, :tar],
          applications: [
            runtime_tools: :permanent,
            sak_resources: :permanent,
            sak_messaging: :permanent,
            common_activities: :permanent,
            deployments: :permanent,
            core: :permanent,
            notifications: :permanent,
            gateway: :permanent
          ]
        ],

        standalone_windows: [
          steps: [:assemble, :tar],
          applications: [
            runtime_tools: :permanent,
            sak_resources: :permanent,
            sak_messaging: :permanent,
            common_activities: :permanent,
            deployments: :permanent,
            core: :permanent,
            notifications: :permanent,
            gateway: :permanent
          ]
        ],

        standalone_termux: [
          steps: [:assemble, :tar],
          applications: [
            runtime_tools: :permanent,
            sak_resources: :permanent,
            sak_messaging: :permanent,
            common_activities: :permanent,
            deployments: :permanent,
            core: :permanent,
            notifications: :permanent,
            gateway: :permanent
          ]
        ],

        embedded_raspberry_pi: [
          steps: [:assemble, :tar],
          applications: [
            runtime_tools: :permanent,
            sak_resources: :permanent,
            sak_messaging: :permanent,
            common_activities: :permanent,
            deployments: :permanent,
            core: :permanent,
            notifications: :permanent,
            gateway: :permanent
          ]
        ]
      ]
    ]
  end

  defp deps do
    [
      {:excoveralls, "~> 0.10", only: [:test]}
    ]
  end

  defp aliases do
    [
      "all.integration": [
        "cmd --app sak_resources mix integration",
        "cmd --app sak_messaging mix integration",
        "cmd --app common_activities mix integration",
        "cmd --app core mix integration",
        "cmd --app notifications mix integration",
        "cmd --app deployments mix integration",
        "cmd --app gateway mix integration"
      ],
      "all.unit": [
        "cmd --app sak_resources mix unit",
        "cmd --app sak_messaging mix unit",
        "cmd --app common_activities mix unit",
        "cmd --app core mix unit",
        "cmd --app notifications mix unit",
        "cmd --app deployments mix unit",
        "cmd --app gateway mix unit"
      ],
      "all.proto": [
        "cmd --app sak_resources mix proto",
        "cmd --app sak_messaging mix proto",
        "cmd --app common_activities mix proto",
        "cmd --app core mix proto",
        "cmd --app notifications mix proto",
        "cmd --app deployments mix proto",
        "cmd --app gateway mix proto"
      ],
      "all.coverage": [
        "cmd --app sak_resources mix coveralls.unit",
        "cmd --app sak_messaging mix coveralls.unit",
        "cmd --app common_activities mix coveralls.unit",
        "cmd --app core mix coveralls.unit",
        "cmd --app notifications mix coveralls.unit",
        "cmd --app deployments mix coveralls.unit",
        "cmd --app gateway mix coveralls.unit"
      ]
    ]
  end
end
