 - Deployments:
   - QA: Add more coverage over ManifestScroll.update: It should return :ok on success.
   - Fix: Correct the dependency problem in DefaultEventEmitter:
   it uses a function called "to_command_response". That is to say,
   the event emitter is maling command response payloads
   to use as event payloads.
   - Refactor: Review packaging
   - Refactor: Review response mapping for all operations and events
   - Fix: Correct start_link option handling on all GenServers
   - Feature: handle malformed payloads
   - Feature: handle business errors
 - Notifications:
   - Fix: Correct start_link option handling on all GenServers
   - Feature: handle malformed payloads
   - Feature: handle business errors
 - Gateway:
   - Fix: Correct start_link option handling on all GenServers
   - Feature: handle malformed payloads
   - Feature: handle business errors
 - Deployments:
   - Fix: Correct start_link option handling on all GenServers
   - Feature: handle malformed payloads
   - Feature: handle business errors
- Core:
   - Fix: Correct start_link option handling on all GenServers
   - Feature: handle malformed payloads
   - Feature: handle business errors
